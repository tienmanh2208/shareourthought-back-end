<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeNullableForUserResolveOfReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reports', function (Blueprint $table) {
            $table->unsignedInteger('user_resolve')->nullable()->change();
            $table->unsignedInteger('report_info_id')->nullable()->change();
            $table->text('reason')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports', function (Blueprint $table) {
            $table->unsignedInteger('user_resolve')->change();
            $table->unsignedInteger('report_info_id')->change();
            $table->string('reason', 45)->change();
        });
    }
}

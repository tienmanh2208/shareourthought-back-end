<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToTableGroupInfos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('group_infos', function (Blueprint $table) {
            $table->string('description')->after('key');
            $table->string('group_id')->after('creator');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('group_infos', function (Blueprint $table) {
            $table->dropColumn('description');
            $table->dropColumn('group_id');
        });
    }
}

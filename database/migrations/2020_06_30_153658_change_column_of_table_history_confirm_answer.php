<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnOfTableHistoryConfirmAnswer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('history_confirm_answer', function (Blueprint $table) {
            $table->renameColumn('answers_questions_id', 'answer_id');
            $table->renameColumn('users_id', 'user_id');
            $table->renameColumn('answers_users_id', 'answerer_id');
            $table->tinyInteger('action')->after('users_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('history_confirm_answer', function (Blueprint $table) {
            $table->dropColumn('action');
            $table->renameColumn('answer_id', 'answers_questions_id');
            $table->renameColumn('user_id', 'users_id');
            $table->renameColumn('answerer_id', 'answers_users_id');
        });
    }
}

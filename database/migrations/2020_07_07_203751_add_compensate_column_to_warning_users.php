<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCompensateColumnToWarningUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('warning_users', function (Blueprint $table) {
            $table->tinyInteger('compensate_status')->after('reporter_id')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('warning_users', function (Blueprint $table) {
            $table->dropColumn('compensate_status');
        });
    }
}

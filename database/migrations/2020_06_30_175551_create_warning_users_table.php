<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWarningUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warning_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('user_id');
            $table->tinyInteger('warning_type');
            $table->unsignedInteger('reporter_id');
            $table->string('question_id')->nullable();
            $table->string('answer_id')->nullable();
            $table->string('comment_id')->nullable();
            $table->timestamps();

            $table->foreign('user_id', 'warning_users_user_id_id')
                ->references('id')->on('users');

            $table->foreign('reporter_id', 'warning_users_reporter_id_id')
                ->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warning_users');
    }
}

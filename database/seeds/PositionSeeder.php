<?php

use App\Enums\AdminPosition;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PositionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $time = Carbon::now();

        DB::table('positions')->insert([
            ['id' => AdminPosition::QUESTION_MANAGEMENT, 'position_name' => 'Quản trị câu hỏi', 'description' => '', 'created_at' => $time, 'updated_at' => $time],
            ['id' => AdminPosition::ANSWER_MANAGEMENT, 'position_name' => 'Quản trị câu trả lời', 'description' => '', 'created_at' => $time, 'updated_at' => $time],
            ['id' => AdminPosition::USER_MANAGEMENT, 'position_name' => 'Quản trị người dùng', 'description' => '', 'created_at' => $time, 'updated_at' => $time],
            ['id' => AdminPosition::ADMIN_MANAGEMENT, 'position_name' => 'Quản trị admin', 'description' => '', 'created_at' => $time, 'updated_at' => $time],
        ]);
    }
}

<?php

use App\Enums\AdminRole;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $time = Carbon::now();

        DB::table('roles')->insert([
            ['id' => AdminRole::QUESTION_DELETE, 'name' => 'Xóa câu hỏi', 'description' => '', 'created_at' => $time, 'updated_at' => $time],
            ['id' => AdminRole::QUESTION_EDIT, 'name' => 'Sửa câu hỏi', 'description' => '', 'created_at' => $time, 'updated_at' => $time],
            ['id' => AdminRole::ANSWER_DELETE, 'name' => 'Xóa câu trả lời', 'description' => '', 'created_at' => $time, 'updated_at' => $time],
            ['id' => AdminRole::ANSWER_EDIT, 'name' => 'Sửa câu trả lời', 'description' => '', 'created_at' => $time, 'updated_at' => $time],
            ['id' => AdminRole::ANSWER_CHANGE_STATUS, 'name' => 'Cập nhật trạng thái câu trả lời', 'description' => '', 'created_at' => $time, 'updated_at' => $time],
            ['id' => AdminRole::USER_EDIT, 'name' => 'Sửa thông tin người dùng', 'description' => '', 'created_at' => $time, 'updated_at' => $time],
            ['id' => AdminRole::USER_BAN, 'name' => 'Ban người dùng', 'description' => '', 'created_at' => $time, 'updated_at' => $time],
            ['id' => AdminRole::USER_DELETE, 'name' => 'Xóa người dùng', 'description' => '', 'created_at' => $time, 'updated_at' => $time],
            ['id' => AdminRole::USER_CREATE, 'name' => 'Tạo người dùng', 'description' => '', 'created_at' => $time, 'updated_at' => $time],
            ['id' => AdminRole::ADMIN_CREATE, 'name' => 'Tạo quản trị viên', 'description' => '', 'created_at' => $time, 'updated_at' => $time],
            ['id' => AdminRole::ADMIN_EDIT, 'name' => 'Sửa thông tin quản trị viên', 'description' => '', 'created_at' => $time, 'updated_at' => $time],
        ]);
    }
}

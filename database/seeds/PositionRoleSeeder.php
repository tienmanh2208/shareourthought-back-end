<?php

use App\Enums\AdminPosition;
use App\Enums\AdminRole;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PositionRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $time = Carbon::now();

        DB::table('position_role')->insert([
            ['position_id' => AdminPosition::QUESTION_MANAGEMENT, 'roles_id' => AdminRole::QUESTION_DELETE, 'created_at' => $time, 'updated_at' => $time],
            ['position_id' => AdminPosition::QUESTION_MANAGEMENT, 'roles_id' => AdminRole::QUESTION_EDIT, 'created_at' => $time, 'updated_at' => $time],
            ['position_id' => AdminPosition::ANSWER_MANAGEMENT, 'roles_id' => AdminRole::ANSWER_EDIT, 'created_at' => $time, 'updated_at' => $time],
            ['position_id' => AdminPosition::ANSWER_MANAGEMENT, 'roles_id' => AdminRole::ANSWER_DELETE, 'created_at' => $time, 'updated_at' => $time],
            ['position_id' => AdminPosition::ANSWER_MANAGEMENT, 'roles_id' => AdminRole::ANSWER_CHANGE_STATUS, 'created_at' => $time, 'updated_at' => $time],
            ['position_id' => AdminPosition::USER_MANAGEMENT, 'roles_id' => AdminRole::USER_EDIT, 'created_at' => $time, 'updated_at' => $time],
            ['position_id' => AdminPosition::USER_MANAGEMENT, 'roles_id' => AdminRole::USER_BAN, 'created_at' => $time, 'updated_at' => $time],
            ['position_id' => AdminPosition::USER_MANAGEMENT, 'roles_id' => AdminRole::USER_DELETE, 'created_at' => $time, 'updated_at' => $time],
            ['position_id' => AdminPosition::USER_MANAGEMENT, 'roles_id' => AdminRole::USER_CREATE, 'created_at' => $time, 'updated_at' => $time],
            ['position_id' => AdminPosition::ADMIN_MANAGEMENT, 'roles_id' => AdminRole::ADMIN_CREATE, 'created_at' => $time, 'updated_at' => $time],
            ['position_id' => AdminPosition::ADMIN_MANAGEMENT, 'roles_id' => AdminRole::ADMIN_EDIT, 'created_at' => $time, 'updated_at' => $time],
        ]);
    }
}

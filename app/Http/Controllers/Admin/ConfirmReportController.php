<?php

namespace App\Http\Controllers\Admin;

use App\Enums\AnswerStatus;
use App\Http\Controllers\Controller;
use App\Services\AdminService;
use App\Services\QuestionService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ConfirmReportController extends Controller
{
    protected $questionService;
    protected $adminService;

    public function __construct(QuestionService $questionService, AdminService $adminService)
    {
        $this->questionService = $questionService;
        $this->adminService = $adminService;
    }

    public function main(Request $request)
    {
        $params = $this->getParams($request);

        if ((int) $params['action'] == AnswerStatus::REJECTED) {
            $this->adminService->confirmAnswerIsWrong($params['answerer_id'], Auth::id(), $params['answer_id']);
        } else {
            $result = $this->questionService->updateAnswerStatusForAdmin(
                $params['answer_id'],
                $params['question_id'],
                $params['action']
            );

            if (!$result['status']) {
                return response()->json([
                    'code' => 400,
                    'message' => $result['message']
                ], 200);
            }

            $this->adminService->confirmAnswerIsTrue(
                $params['answer_id'],
                Auth::id(),
                $params['answerer_id'],
                $params['questioner_id'],
                $params['question_id'],
                $params['price']
            );
        }

        $this->adminService->updateReportInfo($params['report_id'], $params['action']);

        return response()->json([
            'code' => 203,
            'message' => trans('server_response.report_accept_answer_is_true_successfully')
        ]);
    }

    protected function getParams(Request $request)
    {
        return $request->only(['answerer_id', 'answer_id', 'action', 'report_id', 'question_id', 'questioner_id', 'price']);
    }

    /**
     * Rules for validation
     */
    public function rules()
    {
        return [
            'answer_id' => 'required|string',
            'question_id' => 'required|string',
            'action' => 'required|integer',
            'report_id' => 'required|integer',
            'answerer_id' => 'required|integer',
            'questioner_id' => 'required|integer',
            'price' => 'required|integer',
        ];
    }
}

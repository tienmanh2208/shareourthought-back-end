<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\AdminService;
use App\Services\QuestionService;

class GetOpenReportForAdminController extends Controller
{
    protected $adminService;
    protected $questionService;

    public function __construct(AdminService $adminService, QuestionService $questionService)
    {
        $this->adminService = $adminService;
        $this->questionService = $questionService;
    }

    public function main()
    {
        $listReport = $this->adminService->getListOpenReport();
        $listId = $this->getListIdContent($listReport['data']);
        $listQuestion = $this->questionService->getListQuestionByListAnswerId($listId);

        if (!$listQuestion['status']) {
            return response()->json([
                'code' => 400,
                'message' => $listQuestion['message']
            ]);
        }

        return response()->json([
            'code' => 200,
            'data' => $this->mergeListQuestionWithListReport($listReport, $listQuestion['data'])
        ]);
    }

    /**
     * Get list id content from list open report
     * 
     * @param array $listContent
     */
    protected function getListIdContent(array $listContent)
    {
        return array_map(function ($item) {
            return $item['id_content'];
        }, $listContent);
    }

    /**
     * Merge list report with the content of question and answer
     * 
     * @param array $listReport
     * @param array $listQuestion
     */
    protected function mergeListQuestionWithListReport(array $listReport, array $listQuestion)
    {
        $questionInfo = [];

        foreach ($listQuestion as $question) {
            $questionInfo[$question->_id] = $question;
        }

        foreach($listReport['data'] as &$report) {
            $report['content_info'] = $questionInfo[$report['id_content']];
        }

        return $listReport;
    }
}

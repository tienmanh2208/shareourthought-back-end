<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\AdminService;
use App\Services\UserService;
use Illuminate\Support\Facades\Auth;

class GetInfoAdminController extends Controller
{
    protected $userService;
    protected $adminService;

    public function __construct(UserService $userService, AdminService  $adminService)
    {
        $this->userService = $userService;
        $this->adminService = $adminService;
    }

    public function main()
    {
        $listPosition = $this->adminService->getListPosition(Auth::id())->toArray();

        return response()->json([
            'code' => 200,
            'data' => Auth::user()->toArray()
                + Auth::user()->additionalInfo->toArray()
                + ['positions' => $listPosition]
        ]);
    }
}

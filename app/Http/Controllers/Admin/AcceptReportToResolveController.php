<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\AdminService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AcceptReportToResolveController extends Controller
{
    protected $adminService;

    public function __construct(AdminService $adminService)
    {
        $this->adminService = $adminService;
    }

    public function main(Request $request)
    {
        $response = $this->adminService->acceptReport(Auth::id(), $request->report_id);

        if (!$response['status']) {
            return response()->json([
                'code' => 400,
                'message' => $response['message']
            ]);
        }

        return response()->json([
            'code' => 203,
            'message' => trans("server_response.report_accept_successfully")
        ]);
    }
}

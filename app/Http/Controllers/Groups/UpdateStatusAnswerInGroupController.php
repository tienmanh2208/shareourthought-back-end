<?php

namespace App\Http\Controllers\Groups;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\GroupService;
use App\Services\QuestionService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UpdateStatusAnswerInGroupController extends Controller
{
    protected $questionService;
    protected $groupService;

    public function __construct(QuestionService $questionService, GroupService $groupService)
    {
        $this->questionService = $questionService;
        $this->groupService = $groupService;
    }

    public function main(Request $request)
    {
        $params = $this->getParams($request);

        $validation = Validator::make($params, $this->rules());

        if ($validation->fails()) {
            return response()->json([
                'code' => 400,
                'message' => $validation->errors()->first(),
            ], 200);
        }

        if (!$this->groupService->doesUserBelongToGroup($params['group_id'], Auth::id())) {
            return response()->json([
                'code' => 403,
                'message' => trans('server_response.request_forbidden')
            ], 200);
        }

        $result = $this->questionService->updateStatusAnswer(
            $params['answer_id'],
            $params['question_id'],
            Auth::id(),
            $params['status']
        );

        if (!$result['status']) {
            return response()->json([
                'code' => 400,
                'message' => $result['message']
            ], 200);
        }

        return response()->json([
            'code' => 203,
            'message' => trans('server_response.update_status_answer_successfully')
        ]);
    }

    /**
     * Get params from request
     */
    public function getParams(Request $request)
    {
        return $request->only('answer_id', 'question_id', 'status', 'group_id');
    }

    /**
     * Rules for validation
     */
    public function rules()
    {
        return [
            'answer_id' => 'required|string',
            'question_id' => 'required|string',
            'status' => 'required|integer',
            'group_id' => 'required|integer',
        ];
    }
}

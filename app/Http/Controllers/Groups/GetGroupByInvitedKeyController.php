<?php

namespace App\Http\Controllers\Groups;

use App\Http\Controllers\Controller;
use App\Services\GroupService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GetGroupByInvitedKeyController extends Controller
{
    protected $groupService;
    protected $userService;

    public function __construct(GroupService $groupService, UserService $userService)
    {
        $this->groupService = $groupService;
        $this->userService = $userService;
    }

    /**
     * Get group by invited key
     */
    public function main(Request $request)
    {
        $params = $this->getParams($request);

        if (count($params) == 0) {
            return response()->json([
                'code' => 400,
                'message' => trans('server_response.invited_key_is_missing')
            ]);
        }

        $groupInfo = $this->groupService->getGroupInfoByInvitedKey($params['invitedKey']);
        $userInfo = $this->userService->getUserByUserId($groupInfo->creator);

        if (is_null($groupInfo)) {
            return response()->json([
                'code' => 400,
                'message' => trans('server_response.group_not_found_by_invited_key'),
            ], 200);
        }

        return response()->json([
            'code' => 200,
            'data' => $groupInfo->toArray() + ['creator_name' => $userInfo->last_name . ' ' . $userInfo->first_name]
        ], 200);
    }

    /**
     * Get params from request
     * 
     * @return array [
     *  invitedKey => string
     * ]
     */
    public function getParams(Request $request)
    {
        return $request->only(['invitedKey']);
    }
}

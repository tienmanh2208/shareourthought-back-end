<?php

namespace App\Http\Controllers\Groups;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\QuestionService;

class GetAllQuestionOfGroupController extends Controller
{
    protected $questionService;

    public function __construct(QuestionService $questionService)
    {
        $this->questionService = $questionService;
    }

    public function main($groupId, Request $request)
    {
        $result = $this->questionService->getListQuestionOfGroup($groupId, $request->section_id);

        if ($result['status']) {
            return response()->json([
                'code' => 200,
                'data' => $result['data']
            ], 200);
        }

        return response()->json([
            'code' => 400,
            'message' => $result['message']
        ], 200);
    }
}

<?php

namespace App\Http\Controllers\Groups;

use App\Http\Controllers\Controller;
use App\Services\GroupService;
use App\Services\QuestionService;

class GetAllQuestionOfUserInGroupController extends Controller
{
    protected $groupService;
    protected $questionService;

    public function __construct(GroupService $groupService, QuestionService $questionService)
    {
        $this->groupService = $groupService;
        $this->questionService = $questionService;
    }

    public function main($groupId, $userId)
    {
        if (!$this->groupService->doesUserBelongToGroup($groupId, $userId)) {
            return response()->json([
                'code' => 403,
                'message' => trans('server_response.request_forbidden')
            ], 200);
        }

        $result = $this->questionService->getListQuestionOfUserInGroup($userId, $groupId);

        if ($result['status']) {
            return response()->json([
                'code' => 200,
                'data' => $result['data']
            ]);
        }

        return response()->json([
            'code' => 400,
            'message' => $result['message']
        ], 200);
    }
}

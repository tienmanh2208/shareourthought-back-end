<?php

namespace App\Http\Controllers\Groups;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\GroupService;
use App\Services\QuestionService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CreateQuestionForGroupController extends Controller
{
    protected $groupService;
    protected $questionService;

    public function __construct(GroupService $groupService, QuestionService $questionService)
    {
        $this->groupService = $groupService;
        $this->questionService = $questionService;
    }

    public function main(Request $request)
    {
        $params = $this->getParams($request);

        $validation = Validator::make($params, $this->rules());

        if ($validation->fails()) {
            return response()->json([
                'code' => 400,
                'message' => $validation->errors()->first(),
            ], 200);
        }

        if (!$this->groupService->doesUserBelongToGroup($params['group_id'], Auth::id())) {
            return response()->json([
                'code' => 403,
                'message' => trans('server_response.request_forbidden')
            ], 200);
        }

        $response = $this->createQuestion(
            $params['question_content']['title'],
            $params['question_content']['content'],
            $params['section_id'],
            $params['tags'],
            $params['group_id'],
            $params['field_id']
        );

        if (!$response['status']) {
            return response()->json([
                'code' => 400,
                'message' => $response['message']
            ]);
        }

        $this->groupService->increaseQuestionCount($params['group_id'], $params['section_id']);

        return response()->json([
            'code' => 201,
            'message' => trans('server_response.create_question_successfullly'),
            'data' => [
                'question_id' => $response['question_id'],
            ]
        ]);
    }

    /**
     * Call to create question for group
     */
    protected function createQuestion(string $title, string $content, int $sectionId, array $tags, int $groupId, int $fieldId)
    {
        $groupInfo = $this->groupService->getGroupInfo($groupId);

        return $this->questionService->createQuestion(
            Auth::id(),
            $title,
            $content,
            $groupInfo['default_coin'],
            1,
            $fieldId,
            $tags,
            $groupId,
            $sectionId
        );
    }

    /**
     * Get params from request
     * 
     * @param Request $request
     * @return array
     */
    protected function getParams(Request $request)
    {
        return $request->only('question_content', 'section_id', 'tags', 'group_id', 'field_id');
    }

    /**
     * Rules for validation
     */
    protected function rules()
    {
        return [
            'question_content' => 'required|array',
            'question_content.title' => 'required|string',
            'question_content.content' => 'required|string',
            'section_id' => 'required|integer',
            'tags' => 'array',
            'group_id' => 'required|integer',
            'field_id' => 'required|integer',
        ];
    }
}

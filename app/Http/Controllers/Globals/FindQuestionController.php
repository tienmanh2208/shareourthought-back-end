<?php

namespace App\Http\Controllers\Globals;

use App\Http\Controllers\Controller;
use App\Services\QuestionService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FindQuestionController extends Controller
{
    protected $questionService;
    protected $userService;

    public function __construct(QuestionService $questionService, UserService $userService)
    {
        $this->questionService = $questionService;
        $this->userService = $userService;
    }

    public function main(Request $request)
    {
        $params = $this->getParams($request);

        $checkValidation = Validator::make($params, $this->rules());

        if ($checkValidation->fails()) {
            return response()->json([
                'code' => 400,
                'message' => $checkValidation->errors()->first()
            ]);
        }

        $userId = is_null($params['username']) ? null : $this->userService->getUserIdByUsername($params['username']);
        $tags = explode(',', $params['tags']);

        $response = $this->questionService->findQuestion(
            $params['title'],
            $params['content'],
            $tags[0] === '' ? null : $tags,
            $params['price_from'],
            $params['price_to'],
            $userId,
            $params['field_id'],
            $request->page
        );

        return response()->json([
            'code' => 200,
            'data' => $response['data']
        ], 200);
    }

    protected function getParams(Request $request)
    {
        return $request->only(['title', 'content', 'tags', 'price_from', 'price_to', 'username', 'field_id']);
    }

    protected function rules()
    {
        return [
            'title' => 'string|nullable',
            'content' => 'string|nullable',
            'tags' => 'string|nullable',
            'price_from' => 'integer|nullable',
            'price_to' => 'integer|nullable',
            'username' => 'integer|nullable',
            'field_id' => 'integer|nullable'
        ];
    }
}

<?php

namespace App\Http\Controllers\Globals;

use App\Http\Controllers\Controller;
use App\Services\QuestionService;

class GetQuestionDetailController extends Controller
{
    protected $questionService;

    public function __construct(QuestionService $questionService)
    {
        $this->questionService = $questionService;
    }

    public function main($questionId)
    {
        $questionDetailResponse = $this->questionService->getQuestionDetail(0, $questionId);

        if ($questionDetailResponse['status']) {
            return response()->json([
                'code' => 200,
                'data' => $questionDetailResponse['data']
            ], 200);
        }

        return response()->json([
            'code' => 400,
            'message' => $questionDetailResponse['message']
        ], 200);
    }
}

<?php

namespace App\Http\Controllers\Globals;

use App\Http\Controllers\Controller;
use App\Services\QuestionService;

class GetAnswersQuestionController extends Controller
{
    protected $questionService;

    public function __construct(QuestionService $questionService)
    {
        $this->questionService = $questionService;
    }

    public function main(string $questionId)
    {
        $listAnswerResponse = $this->questionService->getListAnswerOfQuestion(0, $questionId);

        if ($listAnswerResponse['status']) {
            return response()->json([
                'code' => 200,
                'data' => $listAnswerResponse['data']
            ], 200);
        }

        return response()->json([
            'code' => 400,
            'message' => $listAnswerResponse['message']
        ], 200);
    }
}

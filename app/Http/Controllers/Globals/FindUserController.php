<?php

namespace App\Http\Controllers\Globals;

use App\Http\Controllers\Controller;
use App\Services\UserService;

class FindUserController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function main($userId)
    {
        return response()->json([
            'code' => 200,
            'data' => $this->userService->findUserByUserId($userId)
        ], 200);
    }
}

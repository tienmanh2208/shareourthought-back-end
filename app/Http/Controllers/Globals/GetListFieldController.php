<?php

namespace App\Http\Controllers\Globals;

use App\Http\Controllers\Controller;
use App\Services\GlobalService;

class GetListFieldController extends Controller
{
    protected $globalService;

    public function __construct(GlobalService $globalService)
    {
        $this->globalService = $globalService;
    }

    public function main()
    {
        return response()->json([
            'code' => 200,
            'data' => $this->globalService->getAllField()
        ]);
    }
}

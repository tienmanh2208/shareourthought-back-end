<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Services\QuestionService;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UpdateContentQuestionController extends Controller
{
    protected $questionService;

    public function __construct(QuestionService $questionService)
    {
        $this->questionService = $questionService;
    }

    public function main(Request $request)
    {
        $params = $this->getParams($request);

        $validation = Validator::make($params, $this->rules());

        if ($validation->fails()) {
            return [
                'code' => 400,
                'message' => $validation->errors()->first(),
            ];
        }

        $response = $this->questionService->updateContentQuestion(Auth::id(), $params['question_id'], $params['content']);

        if (!$response['status']) {
            return response()->json([
                'code' => 400,
                'message' => $response['message']
            ], 200);
        }

        return response()->json([
            'code' => 203,
            'message' => trans('server_response.question_update_content_successfully')
        ], 200);
    }

    /**
     * Get params from Request
     * 
     * @param Request $request
     */
    public function getParams(Request $request)
    {
        return $request->only(['question_id', 'content']);
    }

    /**
     * Rules for validation
     */
    public function rules()
    {
        return [
            'question_id' => 'required|string',
            'content' => 'required|array',
            'content.title' => 'required|string',
            'content.content' => 'required|string',
        ];
    }
}

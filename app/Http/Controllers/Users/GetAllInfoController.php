<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Services\QuestionService;
use App\Services\UserService;
use Illuminate\Support\Facades\Auth;

class GetAllInfoController extends Controller
{
    protected $userService;
    protected $questionService;

    public function __construct(UserService $userService, QuestionService $questionService)
    {
        $this->userService = $userService;
        $this->questionService = $questionService;
    }

    public function main()
    {
        $userInfo = $this->userService->getAllInfoOfUser(Auth::id());
        $qaInfo = $this->questionService->getUserInfo(Auth::id());

        if (!$qaInfo['status']) {
            return response()->json([
                'code' => 400,
                'message' => $qaInfo['message']
            ], 200);
        }

        return response()->json([
            'code' => 200,
            'data' => $userInfo + (array) $qaInfo['data']
        ]);
    }
}

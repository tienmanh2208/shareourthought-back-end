<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Services\QuestionService;
use Illuminate\Support\Facades\Auth;

class GetQuestionDetailController extends Controller
{
    protected $questionService;

    public function __construct(QuestionService $questionService)
    {
        $this->questionService = $questionService;
    }

    public function main(string $questionId)
    {
        $questionDetailResponse = $this->questionService->getQuestionDetail(Auth::id(), $questionId);

        if ($questionDetailResponse['status']) {
            return response()->json([
                'code' => 200,
                'data' => $questionDetailResponse['data']
            ], 200);
        }

        return response()->json([
            'code' => 400,
            'message' => $questionDetailResponse['message']
        ], 200);
    }
}

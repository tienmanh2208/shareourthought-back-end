<?php

namespace App\Http\Controllers\Users;

use App\Enums\AnswerReportStatus;
use App\Enums\ReportType;
use App\Http\Controllers\Controller;
use App\Services\QuestionService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UpdateReportStatusAnswerController extends Controller
{
    protected $questionService;
    protected $userService;

    public function __construct(QuestionService $questionService, UserService $userService)
    {
        $this->questionService = $questionService;
        $this->userService = $userService;
    }

    public function main(Request $request)
    {
        $response = $this->questionService->updateReportStatus($request->answerId, AnswerReportStatus::REPORTED);

        if (!$response['status']) {
            return response()->json([
                'code' => 400,
                'message' => $response['message']
            ], 200);
        }

        $this->userService->createReportForUser(Auth::id(), $request->answerId, ReportType::ANSWER, $request->reason);

        return response()->json([
            'code' => 201,
            'message' => trans('server_response.create_report_successfully')
        ], 200);
    }
}

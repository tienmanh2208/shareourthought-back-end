<?php

namespace App\Http\Controllers\Users;

use App\Services\QuestionService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class GetListAnswerOfQuestionController extends Controller
{
    protected $questionService;

    public function __construct(QuestionService $questionService)
    {
        $this->questionService = $questionService;
    }

    public function main(string $questionId)
    {
        $listAnswerResponse = $this->questionService->getListAnswerOfQuestion(Auth::id(), $questionId);

        if ($listAnswerResponse['status']) {
            return response()->json([
                'code' => 200,
                'data' => $listAnswerResponse['data']
            ], 200);
        }

        return response()->json([
            'code' => 400,
            'message' => $listAnswerResponse['message']
        ], 200);
    }
}

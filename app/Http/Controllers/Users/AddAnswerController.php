<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\QuestionService;
use App\Services\UserService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AddAnswerController extends Controller
{
    protected $questionService;
    protected $userService;

    public function __construct(QuestionService $questionService, UserService $userService)
    {
        $this->questionService = $questionService;
        $this->userService = $userService;
    }

    public function main(Request $request)
    {
        $params = $this->getParams($request);

        $validation = Validator::make($params, $this->rules());

        if ($validation->fails()) {
            return [
                'code' => 400,
                'message' => $validation->errors()->first(),
            ];
        }

        if ((int) $params['type'] === 1) {
            $response = $this->questionService->addAnswerForQuestion(Auth::id(), $params['question_id'], $params['content']);
        } else {
            $response = $this->questionService->addCommentForQuestion(Auth::id(), $params['question_id'], $params['content']);
        }

        if (!$response['status']) {
            return response()->json([
                'code' => 400,
                'message' => $response['message']
            ]);
        }

        if ((int) $params['type'] === 1) {
            $this->userService->increaseAnswerForUser(Auth::id());
        }

        return response()->json([
            'code' => 201,
            'message' => trans('server_response.add_answer_successfully')
        ]);
    }

    /**
     * Get params from request
     * 
     * type = 1 ~ answer, type = 2 ~ comment
     */
    public function getParams(Request $request)
    {
        return $request->only(['question_id', 'content', 'type']);
    }

    /**
     * Rules for validation
     */
    public function rules()
    {
        return [
            'question_id' => 'required|string',
            'content' => 'required|string',
            'type' => 'required|int',
        ];
    }
}

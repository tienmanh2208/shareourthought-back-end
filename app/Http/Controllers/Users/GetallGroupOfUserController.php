<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Services\GroupService;
use Illuminate\Support\Facades\Auth;

class GetallGroupOfUserController extends Controller
{
    protected $groupService;

    public function __construct(GroupService $groupService)
    {
        $this->groupService = $groupService;
    }

    public function main()
    {
        $listGroup = $this->groupService->getAllGroupOfUser(Auth::id());

        return response()->json([
            'code' => 200,
            'data' => $listGroup
        ], 200);
    }
}

<?php

namespace App\Http\Controllers\Users;

use App\Enums\AnswerStatus;
use App\Enums\ConfirmAnswerAction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\AnswerService;
use App\Services\QuestionService;
use App\Services\UserService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UpdateStatusAnswerController extends Controller
{
    protected $questionService;
    protected $userService;
    protected $answerService;

    public function __construct(QuestionService $questionService, UserService $userService, AnswerService $answerService)
    {
        $this->questionService = $questionService;
        $this->userService = $userService;
        $this->answerService = $answerService;
    }

    public function main(Request $request)
    {
        $params = $this->getParams($request);

        $validation = Validator::make($params, $this->rules());

        if ($validation->fails()) {
            return response()->json([
                'code' => 400,
                'message' => $validation->errors()->first(),
            ], 200);
        }

        $result = $this->questionService->updateStatusAnswer(
            $params['answer_id'],
            $params['question_id'],
            Auth::id(),
            $params['status']
        );

        $this->answerService->saveHistoryConfirmAnswer(
            $params['answer_id'],
            AnswerStatus::CONSIDERING,
            $params['status'],
            Auth::id(),
            ConfirmAnswerAction::BY_QUESTIONER,
            $result['data']
        );

        if (!$result['status']) {
            return response()->json([
                'code' => 400,
                'message' => $result['message']
            ], 200);
        }

        if ($params['status'] == AnswerStatus::ACCEPTED) {
            $this->userService->transferHeldCoinToAnswerer($params['question_id'], $result['data']);
        }

        return response()->json([
            'code' => 203,
            'message' => trans('server_response.update_status_answer_successfully')
        ]);
    }

    /**
     * Get params from request
     */
    public function getParams(Request $request)
    {
        return $request->only('answer_id', 'question_id', 'status');
    }

    /**
     * Rules for validation
     */
    public function rules()
    {
        return [
            'answer_id' => 'required|string',
            'question_id' => 'required|string',
            'status' => 'required|integer',
        ];
    }
}

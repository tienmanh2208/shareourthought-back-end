<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use App\Services\QuestionService;
use App\Http\Controllers\Controller;
use App\Services\UserService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CreateQuestionController extends Controller
{
    protected $questionService;
    protected $userService;

    public function __construct(QuestionService $questionService, UserService $userService)
    {
        $this->questionService = $questionService;
        $this->userService = $userService;
    }

    public function main(Request $request)
    {
        $params = $this->getParams($request);

        $validation = Validator::make($params, $this->rules());

        if ($validation->fails()) {
            return response()->json([
                'code' => 400,
                'message' => $validation->errors()->first(),
            ], 200);
        }

        if (!$this->userService->hasEnoughCoin(Auth::id(), $params['price'])) {
            return response()->json([
                'code' => 400,
                'message' => trans('server_response.remain_coin_is_not_enough')
            ], 200);
        }

        $responseCallQuestionService = $this->questionService->createQuestion(
            Auth::id(),
            $params['question_content']['title'],
            $params['question_content']['content'],
            $params['price'],
            $params['visibility'],
            $params['field_id'],
            $params['tags']
        );

        if (!$responseCallQuestionService['status']) {
            return response()->json([
                'code' => 400,
                'message' => $responseCallQuestionService['message']
            ]);
        }

        $this->userService->increaseQuestionCount(Auth::id(), $params['field_id']);
        $this->userService->holdCoinFromUser(Auth::id(), $responseCallQuestionService['question_id'], $params['price']);

        return response()->json([
            'code' => 201,
            'message' => trans('server_response.create_question_successfullly'),
            'data' => [
                'question_id' => $responseCallQuestionService['question_id'],
            ]
        ]);
    }

    public function getParams(Request $request)
    {
        return $request->only('question_content', 'price', 'visibility', 'field_id', 'tags');
    }

    /**
     * Rules for validation
     */
    public function rules()
    {
        return [
            'question_content' => 'required|array',
            'question_content.title' => 'required|string',
            'question_content.content' => 'required|string',
            'price' => 'required|integer',
            'visibility' => 'required|integer',
            'field_id' => 'required|integer',
            'tags' => 'array',
        ];
    }
}

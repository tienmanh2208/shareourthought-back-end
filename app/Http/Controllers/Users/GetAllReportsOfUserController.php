<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Services\QuestionService;
use App\Services\UserService;
use Illuminate\Support\Facades\Auth;

class GetAllReportsOfUserController extends Controller
{
    protected $userService;
    protected $questionService;

    public function __construct(UserService $userService, QuestionService $questionService)
    {
        $this->userService = $userService;
        $this->questionService = $questionService;
    }

    public function main()
    {
        $listReport = $this->userService->getAllReportOfUser(Auth::id())->toArray();

        $listAnswerId = array_map(function ($report) {
            return $report['id_content'];
        }, $listReport['data']);

        $listAnswerDetail = $this->questionService->getListQuestionByListAnswerId($listAnswerId);

        $convertedAnswerDetail = [];

        foreach ($listAnswerDetail['data'] as $answer) {
            $convertedAnswerDetail[$answer->_id] = $answer->question_id;
        }

        foreach ($listReport['data'] as &$report) {
            $report['question_id'] = $convertedAnswerDetail[$report['id_content']];
        }

        return response()->json([
            'code' => 200,
            'data' => $listReport
        ], 200);
    }
}

<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Services\QuestionService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CompensateForMisjudgingUserController extends Controller
{
    protected $questionService;
    protected $userService;

    public function __construct(QuestionService $questionService, UserService $userService)
    {
        $this->questionService = $questionService;
        $this->userService = $userService;
    }

    public function main(Request $request)
    {
        $params = $this->getParams($request);

        $questionInfo = $this->questionService->getQuestionDetail(Auth::id(), $params['question_id'])['data'];

        if (!$this->userService->hasEnoughCoin(Auth::id(), $questionInfo->price)) {
            return response()->json([
                'code' => 400,
                'message' => trans('server_response.remain_coin_is_not_enough')
            ]);
        };

        $this->userService->decreaseCoinBecauseMisjudgingAnswer(Auth::id(), $questionInfo->price, $params['warning_id']);

        return response()->json([
            'code' => 203,
            'message' => trans('server_response.compensate_successfully')
        ], 200);
    }

    protected function getParams(Request $request)
    {
        return $request->only(['warning_id', 'question_id']);
    }
}

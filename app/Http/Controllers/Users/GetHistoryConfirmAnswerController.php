<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Services\QuestionService;
use App\Services\UserService;
use Illuminate\Support\Facades\Auth;

class GetHistoryConfirmAnswerController extends Controller
{
    protected $userService;
    protected $questionService;

    public function __construct(UserService $userService, QuestionService $questionService)
    {
        $this->userService = $userService;
        $this->questionService = $questionService;
    }

    public function main()
    {
        $listHistory = $this->userService->getHistoryConfirmAnswer(Auth::id());

        if (count($listHistory['data']) === 0) {
            return response()->json([
                'code' => 200,
                'data' => $listHistory
            ], 200);
        }

        return response()->json([
            'code' => 200,
            'data' => $this->addQuestionId($listHistory)
        ], 200);
    }

    protected function addQuestionId(array $listHistory)
    {
        $listAnswerId = array_map(function ($item) {
            return $item['answer_id'];
        }, $listHistory['data']);

        $listQuestion = $this->questionService->getListQuestionByListAnswerId($listAnswerId)['data'];

        $convertedQuestions = [];

        foreach ($listQuestion as $questionInfo) {
            $convertedQuestions[$questionInfo->_id] = $questionInfo->question_id;
        }

        foreach ($listHistory['data'] as &$historyConfirm) {
            $historyConfirm['question_id'] = $convertedQuestions[$historyConfirm['answer_id']];
        }

        return $listHistory;
    }
}

<?php

namespace App\Http\Controllers\Users;

use App\Enums\QuestionVisibility;
use App\Http\Controllers\Controller;
use App\Services\QuestionService;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UpdateVisibilityQuestionController extends Controller
{
    protected $questionService;

    public function __construct(QuestionService $questionService)
    {
        $this->questionService = $questionService;
    }

    public function main(Request $request)
    {
        $params = $this->getParams($request);

        $validation = Validator::make($params, $this->rules());

        if ($validation->fails()) {
            return [
                'code' => 400,
                'message' => $validation->errors()->first(),
            ];
        }

        $response = $this->questionService->updateVisibilityForQuestion(Auth::id(), $params['question_id'], $params['visibility']);

        if (!$response['status']) {
            return response()->json([
                'code' => 400,
                'message' => $response['message']
            ], 200);
        }

        return response()->json([
            'code' => 203,
            'message' => trans('server_response.question_update_visibility_successfully')
        ], 200);
    }

    /**
     * Get params from Request
     * 
     * @param Request $request
     */
    public function getParams(Request $request)
    {
        return $request->only(['visibility', 'question_id']);
    }

    /**
     * Rules for validation
     */
    public function rules()
    {
        return [
            'visibility' => ['required', new EnumValue(QuestionVisibility::class)],
            'question_id' => 'required|string',
        ];
    }
}

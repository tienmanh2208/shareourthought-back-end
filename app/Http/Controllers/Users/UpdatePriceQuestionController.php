<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Services\QuestionService;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UpdatePriceQuestionController extends Controller
{
    protected $questionService;

    public function __construct(QuestionService $questionService)
    {
        $this->questionService = $questionService;
    }

    public function main(Request $request)
    {
        $params = $this->getParams($request);

        $validation = Validator::make($params, $this->rules());

        if ($validation->fails()) {
            return [
                'code' => 400,
                'message' => $validation->errors()->first(),
            ];
        }

        $response = $this->questionService->updatePriceForQuestion(Auth::id(), $params['question_id'], $params['price']);

        if (!$response['status']) {
            return response()->json([
                'code' => 400,
                'message' => $response['message']
            ], 200);
        }

        return response()->json([
            'code' => 203,
            'message' => trans('server_response.question_update_price_successfully')
        ], 200);
    }

    /**
     * Get params from Request
     * 
     * @param Request $request
     */
    public function getParams(Request $request)
    {
        return $request->only(['price', 'question_id']);
    }

    /**
     * Rules for validation
     */
    public function rules()
    {
        return [
            'price' => 'required|int',
            'question_id' => 'required|string',
        ];
    }
}

<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Services\QuestionService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GetAllAnswerOfUserController extends Controller
{
    protected $questionService;

    public function __construct(QuestionService $questionService)
    {
        $this->questionService = $questionService;
    }

    public function main(Request $request)
    {
        return response()->json([
            'code' => 200,
            'data' => $this->questionService->getAllAnswerOfUser(Auth::id(), $request->page)
        ], 200);
    }
}

<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\UserService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class BuyCoinController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function main(Request $request)
    {
        $params = $this->getParams($request);

        $validation = Validator::make($params, $this->rules());

        if ($validation->fails()) {
            return [
                'code' => 400,
                'message' => $validation->errors()->first(),
            ];
        }

        $this->userService->buyCoin(Auth::id(), $params['coin']);

        return response()->json([
            'code' => 203,
            'message' => trans('server_response.buy_coin_successfully')
        ], 200);
    }

    public function getParams(Request $request)
    {
        return $request->only('coin');
    }

    /**
     * Rules for validation
     */
    public function rules()
    {
        return [
            'coin' => 'required|int',
        ];
    }
}

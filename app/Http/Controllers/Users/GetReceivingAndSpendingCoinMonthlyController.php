<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Services\UserService;
use Illuminate\Support\Facades\Auth;

class GetReceivingAndSpendingCoinMonthlyController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function main()
    {
        return response()->json([
            'code' => 200,
            'data' => $this->userService->getSpendingAndReceivingCoinMonthly(Auth::id())
        ]);
    }
}

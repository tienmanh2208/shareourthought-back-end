<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Services\QuestionService;
use Illuminate\Support\Facades\Auth;

class GetAllFieldOfUserController extends Controller
{
    protected $questionService;

    public function __construct(QuestionService $questionService)
    {
        $this->questionService = $questionService;
    }

    public function main()
    {
        return response()->json([
            'code' => 200,
            'data' => $this->questionService->getAllFieldOfAnUser(Auth::id())
        ]);
    }
}

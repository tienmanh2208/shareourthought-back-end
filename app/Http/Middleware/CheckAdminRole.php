<?php

namespace App\Http\Middleware;

use App\Enums\UserRole;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckAdminRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->role != UserRole::ADMIN) {
            return response()->json([
                'code' => 403,
                'message' => trans('server_response.not_admin')
            ]);
        }
        
        return $next($request);
    }
}

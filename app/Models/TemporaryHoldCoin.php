<?php

namespace App\Models;

use App\Enums\TemporaryHoldCoinStatus;
use Illuminate\Database\Eloquent\Model;

class TemporaryHoldCoin extends Model
{
    protected $table = 'temporary_hold_coin';

    protected $fillable = [
        'user_id',
        'amount',
        'question_id',
        'status',
        'coin_after_be_held'
    ];

    /**
     * create new record of temporary hold coin
     */
    public function createTemporaryHoldCoin(int $userId, string $questionId, int $amount, int $remainCoin)
    {
        $this->create([
            'user_id' => $userId,
            'amount' => $amount,
            'question_id' => $questionId,
            'status' => TemporaryHoldCoinStatus::HOLDING,
            'coin_after_be_held' => $remainCoin
        ]);
    }

    /**
     * Get total amount of coin be held in system
     */
    public function getHeldCoinOfUser(int $userId)
    {
        return $this->where('user_id', $userId)
            ->where('status', TemporaryHoldCoinStatus::HOLDING)
            ->selectRaw('sum(amount) as total_held_coin')
            ->first()
            ->total_held_coin;
    }

    /**
     * Get record by question id
     * 
     * @param string $questionId
     */
    public function getRecordByQuestionId(string $questionId)
    {
        return $this->where('question_id', $questionId)
            ->first();
    }

    /**
     * Change status of record
     * 
     * @param string $questionId
     * @param int $status
     */
    public function updateStatusOfRecord(string $questionId, int $status)
    {
        $this->where('question_id', $questionId)
            ->update(['status' => $status]);
    }
}

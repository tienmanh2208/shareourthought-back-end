<?php

namespace App\Models;

use App\Enums\ReportStatus;
use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $table = 'reports';

    protected $fillable = [
        'id_content',
        'report_type',
        'reason',
        'status',
        'user_report',
        'user_resolve',
        'report_info_id',
    ];

    /**
     * Create report for user
     */
    public function createReport(
        string $contentId,
        int $reportType,
        string $reason,
        int $status,
        int $userReport,
        $userResolve = null,
        $reportInfoId = null
    ) {
        $this->create([
            'id_content' => $contentId,
            'report_type' => $reportType,
            'reason' => $reason,
            'status' => $status,
            'user_report' => $userReport,
            'user_resolve' => $userResolve,
            'report_info_id' => $reportInfoId,
        ]);
    }

    /**
     * Get list report with conditions
     */
    public function getListReport($reportStatus, $reportType, $userReport, $userResolve, string $orderBy = 'created_at', string $order = 'DESC', int $perPage = 10)
    {
        $queryBuilder = $this;

        if (!is_null($reportStatus)) {
            $queryBuilder = $queryBuilder->where('status', $reportStatus);
        }

        if (!is_null($reportType)) {
            $queryBuilder = $queryBuilder->where('report_type', $reportType);
        }

        if (!is_null($userReport)) {
            $queryBuilder = $queryBuilder->where('user_report', $userReport);
        }

        if (!is_null($userResolve)) {
            $queryBuilder = $queryBuilder->where('user_resolve', $userResolve);
        }

        return $queryBuilder->orderBy($orderBy, $order)
            ->paginate($perPage);
    }

    /**
     * Get report by Id
     * 
     * @param int $reportId
     */
    public function getReportById(int $reportId)
    {
        return $this->find($reportId);
    }

    /**
     * Add admin to report
     */
    public function addAdminResolveToReport(int $adminId, int $reportId)
    {
        return $this->where('id', $reportId)
            ->update(['user_resolve' => $adminId, 'status' => ReportStatus::IN_PROGRESS]);
    }

    /**
     * Update status of report
     */
    public function updateStatusReport(int $reportId, int $reportInfoId)
    {
        return $this->where('id', $reportId)
            ->update([
                'status' => ReportStatus::DONE,
                'report_info_id' => $reportInfoId
            ]);
    }

    /**
     * Get all report of user
     * 
     * @param int $userId
     */
    public function getAllReportOfUser(int $userId)
    {
        return $this->where('user_report', $userId)
            ->orderBy('created_at', 'DESC')
            ->paginate(5);
    }
}

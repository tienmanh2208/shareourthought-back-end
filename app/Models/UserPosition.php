<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserPosition extends Model
{
    protected $table = 'user_position';

    protected $fillable = ['users_id', 'position_id'];

    /**
     * Get all position of an admin
     */
    public function getAllPositionOfAdmin(int $adminId)
    {
        return $this->where('users_id', $adminId)
            ->pluck('position_id')
            ->toArray();
    }
}

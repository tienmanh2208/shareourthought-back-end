<?php

namespace App\Models;

use App\Enums\UserRole;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable;
    use HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'password',
        'first_name',
        'date_of_birth',
        'last_name',
        'email',
        'role',
        'account_status',
        'coin_remain'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $numberOfTopUsers = 5;

    public function additionalInfo()
    {
        return $this->hasOne('App\Models\AdditionalInfo');
    }

    public function coinTransactions()
    {
        return $this->hasMany('App\Models\CoinTransaction', 'users_id');
    }

    /**
     * Get user info by username
     * 
     * @param string $username
     */
    public function getUserByUsername(string $username)
    {
        return $this->where('username', $username)->first();
    }

    /**
     * @param string $email
     * @param string $userName
     * @return array
     */
    public static function checkExistenceOfUser(string $email, string $userName)
    {
        if (self::checkEmail($email)) {
            return [
                'status' => false,
                'message' => trans('auth.email_exists')
            ];
        }

        if (self::checkUserName($userName)) {
            return [
                'status' => false,
                'message' => trans('auth.username_exists')
            ];
        }

        return [
            'status' => true,
        ];
    }

    /**
     * Check if an username exists or not
     *
     * @param string $userName
     * @return bool
     */
    public static function checkUserName(string $userName)
    {
        $info = self::where('username', $userName)->first();

        if (is_null($info)) {
            return false;
        }

        return true;
    }

    /**
     * @param string $email
     * @return bool
     */
    public static function checkEmail(string $email)
    {
        $info = self::where('email', $email)->first();

        if (is_null($info)) {
            return false;
        }

        return true;
    }

    public function getTopUsers()
    {
        return $this->join('additional_infos', 'users.id', '=', 'additional_infos.user_id')
            ->where('role', UserRole::USER)
            ->orderBy('total_answers', 'desc')
            ->limit($this->numberOfTopUsers)
            ->get(['first_name', 'last_name', 'id'])
            ->toArray();
    }

    public function checkExistenceOfUserById(int $userId)
    {
        $userInfo = $this->getUserById($userId);

        if (is_null($userInfo)) {
            return false;
        }

        return true;
    }

    /**
     * Get user by id
     */
    public function getUserById(int $userId)
    {
        return $this->where('id', $userId)->first();
    }

    public function addCoin(int $userId, int $amountOfCoin)
    {
        $this->where('id', $userId)
            ->update(['coin_remain' => DB::raw('coin_remain + ' . $amountOfCoin)]);
    }

    /**
     * Decrease Coin of user
     */
    public function decreaseCoin(int $userId, int $amountOfCoin)
    {
        $this->where('id', $userId)
            ->update(['coin_remain' => DB::raw('coin_remain - ' . $amountOfCoin)]);
    }

    /**
     * Get list user name  by list id
     * 
     * @param array $listId
     * 
     * @return [
     *  user_id => full_name
     *  user_id => full_name
     * ]
     */
    public function getListUserIdNameByListId(array $listId)
    {
        return $this->whereIn('id', $listId)
            ->selectRaw('CONCAT(last_name, " ", first_name) as full_name, id')
            ->pluck('full_name', 'id')
            ->toArray();
    }

    /**
     * Get coin transaction of user
     * 
     * @param int $userId
     * @param int $perPage
     */
    public function getCoinTransaction(int $userId, int $perPage)
    {
        return $this->find($userId)
            ->coinTransactions()
            ->paginate($perPage);;
    }
}

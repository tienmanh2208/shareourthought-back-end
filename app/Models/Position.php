<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    protected $table = 'positions';

    protected $fillable = ['position_name', 'description'];

    public function getPositionNameOfListId(array $listPositionId)
    {
        return $this->whereIn('id', $listPositionId)
            ->get(['id', 'position_name']);
    }
}

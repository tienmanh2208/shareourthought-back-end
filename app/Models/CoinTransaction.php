<?php

namespace App\Models;

use App\Enums\CoinTransactionAction;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CoinTransaction extends Model
{
    protected $table = 'coin_transactions';

    protected $fillable = ['users_id', 'amount', 'action'];

    /**
     * Add coin by system
     */
    public function addCoinBySystem(int $userId, int $amountOfCoin)
    {
        $this->create([
            'users_id' => $userId,
            'amount' => $amountOfCoin,
            'action' => CoinTransactionAction::ADD_BY_SYSTEM,
        ]);
    }

    /**
     * Add log buy coin by manual for user
     */
    public function buyCoin(int $userId, int $amountOfCoin)
    {
        $this->create([
            'users_id' => $userId,
            'amount' => $amountOfCoin,
            'action' => CoinTransactionAction::ADD_BY_MANUAL
        ]);
    }

    public function getTotalTransactionInfoOfUser(int $userId)
    {
        $transactionInfo['total_income'] = (int) $this->where('users_id', $userId)->whereIn('action', [
            CoinTransactionAction::ADD_BY_MANUAL,
            CoinTransactionAction::ADD_BY_RIGHT_ANSWER
        ])
            ->sum('amount');

        $transactionInfo['total_outcome'] = (int) $this->where('users_id', $userId)->whereIn('action', [
            CoinTransactionAction::DECREASE_BY_QUESTION_RESOLVE
        ])
            ->sum('amount');

        return $transactionInfo;
    }

    /**
     * Save log for user when received coin from question
     * 
     * @param int $userId
     * @param int $amountOfCoin
     */
    public function receivedCoinFormQuestion(int $userId, int $amountOfCoin)
    {
        $this->create([
            'users_id' => $userId,
            'amount' => $amountOfCoin,
            'action' => CoinTransactionAction::ADD_BY_RIGHT_ANSWER
        ]);
    }

    /**
     * Transfer coin to answerer
     * 
     * @param int $userId
     * @param int $amountOfCoin
     */
    public function transferCoinToAnswerer(int $userId, int $amountOfCoin)
    {
        $this->create([
            'users_id' => $userId,
            'amount' => -$amountOfCoin,
            'action' => CoinTransactionAction::DECREASE_BY_QUESTION_RESOLVE
        ]);
    }

    /**
     * Get receiving coin of an user monthly
     * 
     * @param int $userId
     */
    public function getReceivingCoinOfUserMonthly(int $userId)
    {
        return $this->where('users_id', $userId)
            ->where('amount', '>=', 0)
            ->select(DB::raw('sum(amount) as total_amount, YEAR(created_at) as year, MONTH(created_at) as month'))
            ->groupBy(DB::raw('YEAR(created_at), MONTH(created_at) DESC'))
            ->get()
            ->toArray();
    }

    /**
     * Get receiving coin of an user monthly
     * 
     * @param int $userId
     */
    public function getSpendingCoinOfUserMonthly(int $userId)
    {
        return $this->where('users_id', $userId)
            ->where('amount', '<=', 0)
            ->select(DB::raw('sum(amount) as total_amount, YEAR(created_at) as year, MONTH(created_at) as month'))
            ->groupBy(DB::raw('YEAR(created_at), MONTH(created_at) DESC'))
            ->get()
            ->toArray();
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReportInfo extends Model
{
    protected $table = 'report_info';

    protected $fillable = ['actions'];

    /**
     * Create report info
     */
    public function createReportInfo(int $action)
    {
        return $this->create(['actions' => $action]);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Field extends Model
{
    protected $table = 'fields';

    protected $fillable = ['name', 'question_count'];

    public function getTopField()
    {
        $numberOfField = 5;

        return $this->orderBy('question_count', 'DESC')->limit($numberOfField)->get();
    }

    public function increaseQuestionCount(int $fieldId)
    {
        $this->where('id', $fieldId)
            ->update(['question_count' => DB::raw('question_count + 1')]);
    }

    /**
     * Get all field and their infomation
     */
    public function getAllField()
    {
        return $this->all();
    }
}

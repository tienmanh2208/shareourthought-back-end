<?php

namespace App\Models;

use App\Enums\CompensateStatus;
use Illuminate\Database\Eloquent\Model;

class WarningUser extends Model
{
    protected $table = 'warning_users';

    protected $fillable = [
        'user_id',
        'warning_type',
        'reporter_id',
        'question_id',
        'answer_id',
        'comment_id',
        'compensate_status'
    ];

    /**
     * Create warning for user
     */
    public function createWarningUser(
        int $userId,
        int $warningType,
        int $reporterId,
        $questionId,
        $answerId,
        $commentId
    ) {
        return $this->create([
            'user_id' => $userId,
            'warning_type' => $warningType,
            'reporter_id' => $reporterId,
            'question_id' => $questionId,
            'answer_id' => $answerId,
            'comment_id' => $commentId
        ]);
    }

    /**
     * Get list warning of user
     */
    public function getListWarningOfUser(int $userId)
    {
        return $this->where('user_id', $userId)
            ->where('compensate_status', CompensateStatus::NOT_COMPENSTATE)
            ->orderBy('created_at', 'desc')
            ->paginate(5);
    }

    /**
     * Update compensate status
     */
    public function updateCompensateStatus(int $id, int $compensateStatus)
    {
        return $this->where('id', $id)
            ->update(['compensate_status' => $compensateStatus]);
    }

    /**
     * Get all warning of user
     * 
     * @param int $userId
     */
    public function getAllWarningOfUser(int $userId)
    {
        return $this->where('user_id', $userId)
            ->orderBy('compensate_status', 'DESC')
            ->paginate(5);
    }
}

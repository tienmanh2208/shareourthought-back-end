<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoryConfirmAnswer extends Model
{
    protected $table = 'history_confirm_answer';

    protected $fillable = [
        'answer_id',
        'previous_status',
        'current_status',
        'user_id',
        'action',
        'answerer_id'
    ];

    /**
     * Create history confirm answer
     */
    public function createHistoryConfirmAnswer(
        string $answerId,
        int $previousStatus,
        int $currentStatus,
        int $adminId,
        int $action,
        int $answererId
    ) {
        return $this->create([
            'answer_id' => $answerId,
            'previous_status' => $previousStatus,
            'current_status' => $currentStatus,
            'user_id' => $adminId,
            'action' => $action,
            'answerer_id' => $answererId
        ]);
    }

    /**
     * Get list history confirm answer of user
     * 
     * @param int $userId
     */
    public function getListHistoryConfirmAnswerOfUser(int $userId)
    {
        return $this->where('answerer_id', $userId)
            ->orderBy('created_at', 'desc')
            ->paginate(5);
    }
}

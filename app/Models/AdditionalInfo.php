<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AdditionalInfo extends Model
{
    protected $table = 'additional_infos';

    protected $fillable = [
        'user_id',
        'facebook_account',
        'instagram',
        'introduction',
        'total_upvotes',
        'total_downvotes',
        'total_comments',
        'total_answers',
        'total_questions'
    ];

    /**
     * Increase total question of an user
     */
    public function increaseTotalQuestion(int $userId)
    {
        $this->where('user_id', $userId)
            ->update(['total_questions' => DB::raw('total_questions + 1')]);
    }

    /**
     * Increase total answer by 1
     */
    public function increaseTotalAnswer(int $userId)
    {
        $this->where('user_id', $userId)
            ->update(['total_answers' => DB::raw('total_answers + 1')]);
    }
}

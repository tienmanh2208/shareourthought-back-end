<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class WarningType extends Enum
{
    const MISJUDGING_ANSWER = 1;
}

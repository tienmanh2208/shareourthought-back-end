<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class ReportStatus extends Enum
{
    const OPEN = 0;
    const IN_PROGRESS =   1;
    const DONE = 2;
    const CANCEL = 3;
}

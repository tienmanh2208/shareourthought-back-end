<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class CoinTransactionAction extends Enum
{
    const ADD_BY_MANUAL = 0;
    const ADD_BY_RIGHT_ANSWER = 1;
    const DECREASE_BY_QUESTION_RESOLVE = 2;
    const ADD_BY_SYSTEM = 3;
}

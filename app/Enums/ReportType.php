<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class ReportType extends Enum
{
    const COMMENT = 1;
    const QUESTION = 2;
    const ANSWER = 3;
}

<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class CompensateStatus extends Enum
{
    const NOT_COMPENSTATE = 0;
    const COMPENSATED = 1;
}

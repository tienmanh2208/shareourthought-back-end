<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class TemporaryHoldCoinStatus extends Enum
{
    const HOLDING = 1;
    const TRANSFERED = 2;
    const WITHDREW = 3;
}

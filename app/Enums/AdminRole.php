<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class AdminRole extends Enum
{
    const QUESTION_DELETE = 10;
    const QUESTION_EDIT = 11;
    const ANSWER_DELETE = 20;
    const ANSWER_EDIT = 21;
    const ANSWER_CHANGE_STATUS = 22;
    const USER_EDIT = 30;
    const USER_BAN = 31;
    const USER_DELETE = 32;
    const USER_CREATE = 33;
    const ADMIN_CREATE = 40;
    const ADMIN_EDIT = 41;
}

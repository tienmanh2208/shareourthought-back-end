<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class AdminPosition extends Enum
{
    const QUESTION_MANAGEMENT = 1;
    const ANSWER_MANAGEMENT = 2;
    const USER_MANAGEMENT = 3;
    const ADMIN_MANAGEMENT = 4;
}

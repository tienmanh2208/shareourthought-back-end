<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class ConfirmAnswerAction extends Enum
{
    const BY_QUESTIONER = 1;
    const BY_ADMIN = 2;
}

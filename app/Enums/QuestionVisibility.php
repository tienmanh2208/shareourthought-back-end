<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class QuestionVisibility extends Enum
{
    const ONLY_ME = 0;
    const OPEN = 1;
}

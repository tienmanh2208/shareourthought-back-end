<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class AnswerStatus extends Enum
{
    const WAITING = 1;
    const CONSIDERING = 2;
    const ACCEPTED = 3;
    const REJECTED = 4;
}

<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class AnswerReportStatus extends Enum
{
    const NOT_BE_REPORTED = 0;
    const REPORTED = 1;
    const RESOLVED_BY_ADMIN = 2;
}

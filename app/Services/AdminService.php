<?php

namespace App\Services;

use App\Enums\AnswerStatus;
use App\Enums\ConfirmAnswerAction;
use App\Enums\ReportStatus;
use App\Enums\ReportType;
use App\Enums\WarningType;
use App\HistoryConfirmAnswer;
use App\Models\CoinTransaction;
use App\Models\Position;
use App\Models\PositionRole;
use App\Models\Report;
use App\Models\ReportInfo;
use App\Models\Role;
use App\Models\User;
use App\Models\UserPosition;
use App\Models\WarningUser;

class AdminService
{
    protected $position;
    protected $role;
    protected $postionRole;
    protected $userPosition;
    protected $user;
    protected $report;
    protected $warningUser;
    protected $historyConfirmAnswer;
    protected $reportInfo;
    protected $coinTransaction;

    public function __construct(
        Position $position,
        Role $role,
        PositionRole $positionRole,
        UserPosition $userPosition,
        User $user,
        Report $report,
        WarningUser $warningUser,
        HistoryConfirmAnswer $historyConfirmAnswer,
        ReportInfo $reportInfo,
        CoinTransaction $coinTransaction
    ) {
        $this->position = $position;
        $this->role = $role;
        $this->postionRole = $positionRole;
        $this->userPosition = $userPosition;
        $this->user = $user;
        $this->report = $report;
        $this->warningUser = $warningUser;
        $this->historyConfirmAnswer = $historyConfirmAnswer;
        $this->reportInfo = $reportInfo;
        $this->coinTransaction = $coinTransaction;
    }

    /**
     * Get list position of an admin
     * 
     * @param int $adminId
     */
    public function getListPosition(int $adminId)
    {
        $listPosition = $this->userPosition->getAllPositionOfAdmin($adminId);
        return $this->position->getPositionNameOfListId($listPosition);
    }

    /**
     * Get list open report for all admin
     */
    public function getListOpenReport()
    {
        return $this->report->getListReport(
            ReportStatus::OPEN,
            null,
            null,
            null
        )->toArray();
    }

    /**
     * Get all report of an admin
     */
    public function getAllReportOfAdmin(int $adminId)
    {
        return $this->report->getListReport(
            ReportStatus::IN_PROGRESS,
            ReportType::ANSWER,
            null,
            $adminId
        )->toArray();
    }

    /**
     * Accept report for admin
     * 
     * @param $adminId
     * @param $reportId
     */
    public function acceptReport(int $adminId, int $reportId)
    {
        $reportInfo = $this->report->getReportById($reportId);

        if (is_null($reportInfo)) {
            return [
                'status' => false,
                'message' => trans("server_response.report_not_found"),
            ];
        }

        if ($reportInfo->user_resolve != null) {
            return [
                'status' => false,
                'message' => trans("server_response.report_was_taken")
            ];
        }

        $this->report->addAdminResolveToReport($adminId, $reportId);
        return [
            'status' => true,
        ];
    }

    /**
     * Create warning for user who misjudged the answer
     */
    public function warningQuestionerForMisjudgingAnswer(
        int $questionerId,
        int $reporterId,
        $questionId = null,
        $answerId = null
    ) {
        return $this->warningUser->createWarningUser(
            $questionerId,
            WarningType::MISJUDGING_ANSWER,
            $reporterId,
            $questionId,
            $answerId,
            null
        );
    }

    /**
     * Add log confirm answer of admin
     */
    public function confirmAnswerIsTrue(string $answerId, int $adminId, int $answererId, string $questionerId, $questionId = null, int $price)
    {
        $this->historyConfirmAnswer->createHistoryConfirmAnswer(
            $answerId,
            AnswerStatus::REJECTED,
            AnswerStatus::ACCEPTED,
            $adminId,
            ConfirmAnswerAction::BY_ADMIN,
            $answererId
        );

        $this->user->addCoin($answererId, $price);
        $this->coinTransaction->transferCoinToAnswerer($adminId, $price);

        $this->warningQuestionerForMisjudgingAnswer($questionerId, $adminId, $questionId, $answerId);
    }

    /**
     * Confirm answer is wrong
     */
    public function confirmAnswerIsWrong(int $answererId, int $adminId, string $answerId)
    {
        $this->historyConfirmAnswer->createHistoryConfirmAnswer(
            $answerId,
            AnswerStatus::REJECTED,
            AnswerStatus::REJECTED,
            $adminId,
            ConfirmAnswerAction::BY_ADMIN,
            $answererId
        );
    }

    public function updateReportInfo(int $reportId, int $action)
    {
        $reportBonusInfo = $this->reportInfo->createReportInfo($action);
        $this->report->updateStatusReport($reportId, $reportBonusInfo->id);
    }
}

<?php

namespace App\Services;

use App\Enums\CompensateStatus;
use App\Enums\ReportStatus;
use App\Enums\TemporaryHoldCoinStatus;
use App\HistoryConfirmAnswer;
use App\Models\AdditionalInfo;
use App\Models\CoinTransaction;
use App\Models\Field;
use App\Models\Report;
use App\Models\TemporaryHoldCoin;
use App\Models\User;
use App\Models\WarningUser;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class UserService
{
    protected $field;
    protected $additionalInfo;
    protected $user;
    protected $coinTransaction;
    protected $temporaryHoldCoin;
    protected $report;
    protected $warningUser;
    protected $historyConfirmAnswer;

    public function __construct(
        Field $field,
        AdditionalInfo $additionalInfo,
        User $user,
        CoinTransaction $coinTransaction,
        TemporaryHoldCoin $temporaryHoldCoin,
        Report $report,
        WarningUser $warningUser,
        HistoryConfirmAnswer $historyConfirmAnswer
    ) {
        $this->field = $field;
        $this->additionalInfo = $additionalInfo;
        $this->user = $user;
        $this->coinTransaction = $coinTransaction;
        $this->temporaryHoldCoin = $temporaryHoldCoin;
        $this->report = $report;
        $this->warningUser = $warningUser;
        $this->historyConfirmAnswer = $historyConfirmAnswer;
    }

    /**
     * Get user info by id
     * 
     * @param int $userId
     */
    public function getUserByUserId(int $userId)
    {
        return $this->user->find($userId);
    }

    /**
     * Increase Question count for user and field when a question is questioned successfully
     * 
     * @param int $userId
     * @param int $fieldId
     */
    public function increaseQuestionCount(int $userId, int $fieldId)
    {
        $this->additionalInfo->increaseTotalQuestion($userId);
        $this->field->increaseQuestionCount($fieldId);
    }

    public function buyCoin(int $userId, int $coin)
    {
        $this->user->addCoin($userId, $coin);
        $this->coinTransaction->buyCoin($userId, $coin);
    }

    /**
     * Get coin info 
     * 
     * @param int $userId
     * 
     * @return
     */
    public function getCoinInfo(int $userId)
    {
        $userInfo = $this->user->getUserById($userId);

        $coinInfo['coin_remain'] = $userInfo->coin_remain;
        $transactionInfo = $this->coinTransaction->getTotalTransactionInfoOfUser($userId);
        $coinInfo['total_income'] = $transactionInfo['total_income'];
        $coinInfo['total_outcome'] = $transactionInfo['total_outcome'];
        $coinInfo['total_held_coin'] = (int) $this->temporaryHoldCoin->getHeldCoinOfUser($userId);

        return $coinInfo;
    }

    /**
     * Increase answer for user
     */
    public function increaseAnswerForUser(int $userId)
    {
        $this->additionalInfo->increaseTotalAnswer($userId);
    }

    /**
     * Check if an user has enough coin to question or not
     * 
     * @param int $userId
     * @param int $coin
     */
    public function hasEnoughCoin(int $userId, int $coin)
    {
        if ($this->user->where('id', $userId)->first()->coin_remain < $coin) {
            return false;
        }

        return true;
    }

    /**
     * Create temporary_hold_coin when create question
     */
    public function holdCoinFromUser(int $userId, string $questionId, int $coin)
    {
        $this->user->decreaseCoin($userId, $coin);
        $userInfo = $this->user->find($userId);
        $this->temporaryHoldCoin->createTemporaryHoldCoin($userId, $questionId, $coin, $userInfo->coin_remain);
    }

    /**
     * Transfer coin to user given the right answer for question
     * 
     * @param string $questionId
     * @param int $answererId
     */
    public function transferHeldCoinToAnswerer(string $questionId, int $answererId)
    {
        $heldCoinInfo = $this->temporaryHoldCoin->getRecordByQuestionId($questionId);
        $this->user->addCoin($answererId, $heldCoinInfo->amount);
        $this->temporaryHoldCoin->updateStatusOfRecord($questionId, TemporaryHoldCoinStatus::TRANSFERED);
        $this->coinTransaction->receivedCoinFormQuestion($answererId, $heldCoinInfo->amount);
        $this->coinTransaction->transferCoinToAnswerer($heldCoinInfo->user_id, $heldCoinInfo->amount);
    }

    /**
     * Find user by user id, do not return sensitive field
     *
     * @param int $userId
     */
    public function findUserByUserId(int $userId)
    {
        $userInfo = $this->user->getUserById($userId);

        if (is_null($userInfo)) {
            return [];
        }

        $additionalInfo = $userInfo->additionalInfo;

        return [
            'full_name' => $userInfo->last_name . ' ' . $userInfo->first_name,
            'date_of_birth' => $userInfo->date_of_birth,
            'email' => $userInfo->email,
            'facebook' => $additionalInfo->facebook_account,
            'instagram' => $additionalInfo->instagram,
            'introduction' => $additionalInfo->introduction
        ];
    }

    /**
     * Get coin transaction of user
     * 
     * @param int $userId
     * @param int $perPage
     */
    public function getCoinTransactionOfUser(int $userId, int $perPage = 10)
    {
        return $this->user->getCoinTransaction($userId, $perPage);
    }

    /**
     * Get receiving coin of an user monthly
     * 
     * @param int $userId
     */
    public function getRecevingCoinMonthly(int $userId)
    {
        $info = $this->coinTransaction->getReceivingCoinOfUserMonthly($userId);

        $result = [];

        foreach ($info as $month) {
            $time = Carbon::createFromDate($month['year'], $month['month'], 1);
            array_push($result, [
                'time' => $time->timestamp * 1000,
                'value' => (int) $month['total_amount']
            ]);
        }

        return $result;
    }

    /**
     * Get spending coin of an user monthly
     * 
     * @param int $userId
     */
    public function getSpendingCoinMonthly(int $userId)
    {
        $info = $this->coinTransaction->getSpendingCoinOfUserMonthly($userId);

        $result = [];
        $arrayMonth = $this->generateMonth(Carbon::now()->firstOfMonth(), 5);

        $spendingInfo = [];

        foreach ($info as $month) {
            $time = Carbon::createFromDate($month['year'], $month['month'], 1);
            $spendingInfo[substr($time->toDateString(), 0, 7)] = (int) $month['total_amount'];
        }

        $result = [];

        foreach ($arrayMonth as $month) {
            array_push($result, [
                'time' => $month,
                'spending' => array_key_exists($month, $spendingInfo) ? $spendingInfo[$month] : 0,
            ]);
        }

        return $result;
    }

    /**
     * Get all info of user, include question, answer
     * 
     * @param int $userId
     */
    public function getAllInfoOfUser(int $userId)
    {
        return Auth::user()->toArray() + Auth::user()->additionalInfo->toArray();
    }

    /**
     * Get summary spending and receiving coin of user
     * 
     * @param int $userId
     */
    public function getSpendingAndReceivingCoinMonthly(int $userId)
    {
        $receivingTransaction = $this->coinTransaction->getReceivingCoinOfUserMonthly($userId);

        $receivingInfo = [];

        foreach ($receivingTransaction as $month) {
            $time = Carbon::createFromDate($month['year'], $month['month'], 1);
            $receivingInfo[substr($time->toDateString(), 0, 7)] = (int) $month['total_amount'];
        }

        $spendingTransaction = $this->coinTransaction->getSpendingCoinOfUserMonthly($userId);

        $spendingInfo = [];

        foreach ($spendingTransaction as $month) {
            $time = Carbon::createFromDate($month['year'], $month['month'], 1);
            $spendingInfo[substr($time->toDateString(), 0, 7)] = (int) $month['total_amount'];
        }

        $arrayMonth = $this->generateMonth(Carbon::now()->firstOfMonth(), 5);

        $result = [];

        foreach ($arrayMonth as $month) {
            array_push($result, [
                'time' => $month,
                'receiving' => array_key_exists($month, $receivingInfo) ? $receivingInfo[$month] : 0,
                'spending' => array_key_exists($month, $spendingInfo) ? $spendingInfo[$month] : 0,
            ]);
        }

        return $result;
    }

    /**
     * Generate list of month
     * 
     * @param string $startMonth 2020-05-00
     * @param int $numberOfMonth
     */
    protected function generateMonth(string $startMonth, int $numberOfMonth)
    {
        $startTime = Carbon::createFromTimeString($startMonth);

        $result = [];

        for ($i = 1; $i <= $numberOfMonth; ++$i) {
            array_push($result, substr($startTime->toDateString(), 0, 7));
            $startTime->subMonth();
        }

        return $result;
    }

    /**
     * Cteate report for user
     * 
     * @param int $userId
     * @param string $contentId
     * @param int $reportType
     * @param string $reason
     */
    public function createReportForUser(int $userId, string $contentId, int $reportType, string $reason)
    {
        $this->report->createReport(
            $contentId,
            $reportType,
            $reason,
            ReportStatus::OPEN,
            $userId
        );
    }

    /**
     * Add coin for user when that user invites guest to use system successfully
     */
    public function addCoinForUserWhenInviteGuest(string $userName)
    {
        $userInfo = $this->user->getUserByUsername($userName);
        if (!is_null($userInfo)) {
            $this->user->addCoin($userInfo->id, 2000);
            $this->coinTransaction->addCoinBySystem($userInfo->id, 2000);
        }
    }

    /**
     * Get user id by username
     * 
     * @param string $username
     */
    public function getUserIdByUsername(string $username)
    {
        $userInfo = $this->user->getUserByUsername($username);

        if (is_null($userInfo)) {
            return null;
        }

        return $userInfo->id;
    }

    /**
     * Get list warning of user
     * 
     * @param int $userId
     */
    public function getListWarningOfUser(int $userId)
    {
        return $this->warningUser->getListWarningOfUser($userId);
    }

    /**
     * Get history confirm
     * 
     * @param $userId
     */
    public function getHistoryConfirmAnswer(int $userId)
    {
        return $this->historyConfirmAnswer->getListHistoryConfirmAnswerOfUser($userId)->toArray();
    }

    /**
     * Decrease coin when a user misjudged an answer
     * 
     * @param int $userId
     * @param int $price
     * @param int $warningId
     */
    public function decreaseCoinBecauseMisjudgingAnswer(int $userId, int $price, int $warningId)
    {
        $this->user->decreaseCoin($userId, $price);
        $this->coinTransaction->transferCoinToAnswerer($userId, $price);
        $this->warningUser->updateCompensateStatus($warningId, CompensateStatus::COMPENSATED);
    }

    /**
     * Get all report of user
     * 
     * @param int $userId
     */
    public function getAllReportOfUser(int $userId)
    {
        return $this->report->getAllReportOfUser($userId);
    }

    /**
     * Get all warning of user
     * 
     * @param int $userId
     */
    public function getAllWarningOfUser(int $userId)
    {
        return $this->warningUser->getAllWarningOfUser($userId);
    }
}

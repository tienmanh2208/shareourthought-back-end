<?php

namespace App\Services;

use App\Models\Field;

class GlobalService
{
    protected $field;

    public function __construct(Field $field)
    {
        $this->field = $field;
    }

    /**
     * Get all field and detail info of each field
     */
    public function getAllField()
    {
        return $this->field->getAllField();
    }
}

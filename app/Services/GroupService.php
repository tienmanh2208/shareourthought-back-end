<?php

namespace App\Services;

use App\Models\Group;
use App\Models\GroupSection;
use App\Models\UserGroup;

class GroupService
{
    protected $group;
    protected $userGroup;
    protected $groupSection;

    public function __construct(Group $group, UserGroup $userGroup, GroupSection $groupSection)
    {
        $this->group = $group;
        $this->userGroup = $userGroup;
        $this->groupSection = $groupSection;
    }

    /**
     * Check if a group id exists or not
     */
    public function checkExistenceGroupId(string $groupId)
    {
        if (is_null($this->group->getGroupInfoByGroupId($groupId))) {
            return false;
        }

        return true;
    }

    /**
     * Get group info
     */
    public function getGroupInfo(int $groupId)
    {
        $groupInfo = $this->group->getGroupInfoForAdmin($groupId);
        $groupInfo['total_member'] = $this->userGroup->getTotalMemberOfGroup($groupId);
        $groupInfo['total_question'] = $this->groupSection->getTotalQuestionOfGroup($groupId);

        return $groupInfo;
    }

    /**
     * Get group info by invited key
     * 
     * @param string $key
     */
    public function getGroupInfoByInvitedKey(string $key)
    {
        $groupInfo = $this->group->getGroupByInvitedKey($key);

        if (is_null($groupInfo)) {
            return null;
        }

        $groupInfo->total_member = $this->userGroup->getTotalMemberOfGroup($groupInfo->id);

        return $groupInfo;
    }

    /**
     * Check if user belongs to group or not
     * 
     * @param int $groupId
     * @param int $userId
     */
    public function doesUserBelongToGroup(int $groupId, int $userId)
    {
        return $this->userGroup->doesMemberBelongToGroup($userId, $groupId);
    }

    /**
     * Increase the question_count of section in group by 1
     * 
     * @param int $groupId
     * @param int $sectionId
     */
    public function increaseQuestionCount(int $groupId, int $sectionId)
    {
        $this->groupSection->increaseQuestionCount($groupId, $sectionId);
    }

    /**
     * Get all group owned by user
     * 
     * @param int $userId
     */
    public function getAllGroupOfUser(int $userId)
    {
        $listGroupId = $this->userGroup->getAllGroupIdOfUser($userId);

        $questionInfo = $this->groupSection->getTotalOfQuestionOfListGroupId($listGroupId)->toArray();

        $convertedQuestionInfo = [];

        foreach ($questionInfo as $info) {
            $convertedQuestionInfo[$info['group_infos_id']] = (int) $info['total_question'];
        }

        $groupInfo = $this->group->getListGroupInfoByListId($listGroupId)->toArray();

        foreach ($groupInfo as &$group) {
            $group['total_question'] = $convertedQuestionInfo[$group['id']];
        }

        return $groupInfo;
    }
}

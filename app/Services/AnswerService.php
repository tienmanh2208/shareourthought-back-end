<?php

namespace App\Services;

use App\HistoryConfirmAnswer;
use App\Http\Controllers\Controller;

class AnswerService extends Controller
{
    protected $historyConfirmAnswer;

    public function __construct(HistoryConfirmAnswer $historyConfirmAnswer)
    {
        $this->historyConfirmAnswer = $historyConfirmAnswer;
    }

    /**
     * Create history confirm answer
     */
    public function saveHistoryConfirmAnswer(
        string $answerId,
        int $previousStatus,
        int $currentStatus,
        int $userId,
        int $action,
        int $answererId
    ) {
        return $this->historyConfirmAnswer->createHistoryConfirmAnswer(
            $answerId,
            $previousStatus,
            $currentStatus,
            $userId,
            $action,
            $answererId
        );
    }
}

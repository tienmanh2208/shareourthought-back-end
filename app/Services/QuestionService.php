<?php

namespace App\Services;

use App\Models\User;

class QuestionService
{
    protected $logService;
    protected $guzzleService;
    protected $user;

    public function __construct(LogService $logService, GuzzleService $guzzleService, User $user)
    {
        $this->logService = $logService;
        $this->guzzleService = $guzzleService;
        $this->user = $user;
    }

    /**
     * Call to guzzle to call question service
     * 
     * @param string $uri
     * @param array $data
     * @param int $statusCode
     * @param string $functionName
     */
    protected function callPostMethodInGuzzle(string $uri, array $data, int $statusCode, string $functionName)
    {
        try {
            $response = $this->guzzleService->post($uri, $data);

            if ($response->code === $statusCode) {
                return [
                    'status' => true,
                    'data' => property_exists($response, 'data') ? $response->data : null,
                ];
            } else {
                return [
                    'status' => false,
                    'message' => $response->message
                ];
            }
        } catch (\Exception $e) {
            $this->logService->writeLogException($functionName, $e);

            return [
                'status' => false,
                'message' => trans('server_response.server_error'),
            ];
        }
    }

    /**
     * Call to guzzle to call question service
     * 
     * @param string $uri
     * @param int $statusCode
     * @param string $functionName
     */
    protected function callGetMethodInGuzzle(string $uri, int $statusCode, string $functionName)
    {
        try {
            $response = $this->guzzleService->get($uri);

            if ($response->code === $statusCode) {
                return [
                    'status' => true,
                    'data' => $response->data
                ];
            } else {
                return [
                    'status' => false,
                    'message' => $response->message
                ];
            }
        } catch (\Exception $e) {
            $this->logService->writeLogException($functionName, $e);

            return [
                'status' => false,
                'message' => trans('server_response.server_error'),
            ];
        }
    }

    /**
     * Get newest question
     */
    public function getNewestQuestion($fieldId, $page)
    {
        $pageInfo = '';
        if (!is_null($page)) {
            $pageInfo = '?page=' . $page;
        }

        if (is_null($fieldId)) {
            $uri = '/api/questions/all' . $pageInfo;
        } else {
            $uri = '/api/questions/all' . $pageInfo . '&field_id=' . $fieldId;
        }

        $response = $this->guzzleService->get($uri);

        if ($response->code != 200) {
            return [
                'code' => 400,
                'message' => trans('server_response.server_error'),
            ];
        }

        return [
            'code' => 200,
            'data' => $response->data,
        ];
    }

    /**
     * Get all questions of users
     */
    public function getQuestionOfUser(int $userId, $fieldId, $page, $perPage)
    {
        $pageInfo = is_null($page) ? 1 : $page;
        $perPageInfo = is_null($perPage) ? '' : '&per_page=' . $perPage;

        if (is_null($fieldId)) {
            $uri = '/api/questions/users/' . $userId . '?page=' . $pageInfo . $perPageInfo;
        } else {
            $uri = '/api/questions/users/' . $userId . '?page=' . $pageInfo . '&field_id=' . $fieldId . $perPageInfo;
        }

        $response = $this->guzzleService->get($uri);

        if ($response->code != 200) {
            return [
                'code' => 400,
                'message' => trans('server_response.server_error'),
            ];
        }

        return [
            'code' => 200,
            'data' => $response->data,
        ];
    }

    /**
     * Create question
     * 
     * @param int $userId
     * @param string $title
     * @param string $content
     * @param int $price
     * @param int $visibility
     * @param int $fieldId
     * @param array $tags
     * @param int $groupId ~ default: null
     * @param int $groupSectionId ~ default: null
     */
    public function createQuestion(int $userId, string $title, string $content, int $price, int $visibility, int $fieldId, array $tags, int $groupId = null, int $groupSectionId = null)
    {
        $uri = '/api/questions/create-question';

        $data = [
            "user_id" => $userId,
            "question_content" => [
                "title" => $title,
                "content" => $content
            ],
            "price" => $price,
            "visibility" => $visibility,
            "field_id" => $fieldId,
            "group" => [
                "group_id" => $groupId,
                "group_section_id" => $groupSectionId
            ],
            "tags" => $tags
        ];

        try {
            $response = $this->guzzleService->post($uri, $data);

            if ($response->code === 201) {
                return [
                    'status' => true,
                    'question_id' => $response->data->question_id
                ];
            } else {
                return [
                    'status' => false,
                    'message' => $response->message
                ];
            }
        } catch (\Exception $e) {
            $this->logService->writeLogException('Call to create question in question service', $e);

            return [
                'status' => false,
                'message' => trans('server_response.server_error'),
            ];
        }
    }

    /**
     * Add answer for a question
     */
    public function addAnswerForQuestion(int $userId, string $questionId, string $content)
    {
        $uri = '/api/questions/create-answer';

        $data = [
            "user_id" => $userId,
            "question_id" => $questionId,
            "content" => $content
        ];

        try {
            $response = $this->guzzleService->post($uri, $data);

            if ($response->code === 201) {
                return [
                    'status' => true,
                ];
            } else {
                return [
                    'status' => false,
                    'message' => $response->message
                ];
            }
        } catch (\Exception $e) {
            $this->logService->writeLogException('Call question service to add answer', $e);

            return [
                'status' => false,
                'message' => trans('server_response.server_error'),
            ];
        }
    }

    /**
     * Add comment for question
     * 
     * @param int $userId
     * @param string $questionId
     * @param string $content
     */
    public function addCommentForQuestion(int $userId, string $questionId, string $content)
    {
        $uri = '/api/questions/add-comment';

        $data = [
            'commentator_id' => $userId,
            'question_id' => $questionId,
            'content' => $content
        ];

        try {
            $response = $this->guzzleService->post($uri, $data);

            if ($response->code === 203) {
                return [
                    'status' => true,
                ];
            } else {
                return [
                    'status' => false,
                    'message' => $response->message
                ];
            }
        } catch (\Exception $e) {
            $this->logService->writeLogException('Call question service to add comment', $e);

            return [
                'status' => false,
                'message' => trans('server_response.server_error'),
            ];
        }
    }

    /**
     * Get question detail
     * 
     * @param int $userId
     * @param string $questionId
     */
    public function getQuestionDetail(int $userId, string $questionId)
    {
        $uri = '/api/questions/' . $questionId . '/detail';

        $data = [
            'user_id' => $userId
        ];

        try {
            $response = $this->guzzleService->post($uri, $data);

            if ($response->code === 200) {
                return [
                    'status' => true,
                    'data' => $this->getUserInfoForQuestion($response->data)
                ];
            } else {
                return [
                    'status' => false,
                    'message' => $response->message
                ];
            }
        } catch (\Exception $e) {
            $this->logService->writeLogException('Call question service to get detail of question', $e);

            return [
                'status' => false,
                'message' => trans('server_response.server_error'),
            ];
        }
    }

    /**
     * Get questionter's name and commentator's name
     * 
     * @param $questionInfo
     */
    protected function getUserInfoForQuestion($questionInfo)
    {
        $listUserId = [];

        array_push($listUserId, $questionInfo->user_id);
        foreach ($questionInfo->comments as $comment) {
            array_push($listUserId, $comment->commentator_id);
        }

        $listUserName = $this->user->getListUserIdNameByListId($listUserId);

        $questionInfo->user_name = $listUserName[$questionInfo->user_id];
        foreach ($questionInfo->comments as &$comment) {
            $comment->commentator_name = $listUserName[$comment->commentator_id];
        }

        return $questionInfo;
    }

    /**
     * Get list answer of a question
     * 
     * @param int $userId
     * @param string $questionId
     */
    public function getListAnswerOfQuestion(int $userId, string $questionId)
    {
        $uri = '/api/answers/all/' . $questionId;

        $data = [
            'user_id' => $userId
        ];

        try {
            $response = $this->guzzleService->post($uri, $data);

            if ($response->code === 200) {
                return [
                    'status' => true,
                    'data' => $this->getUserNameForListAnswer($response->data),
                ];
            } else {
                return [
                    'status' => false,
                    'message' => $response->message
                ];
            }
        } catch (\Exception $e) {
            $this->logService->writeLogException('Call question service to get list answer', $e);

            return [
                'status' => false,
                'message' => trans('server_response.server_error'),
            ];
        }
    }

    /**
     * Get user name for answer
     * 
     * @param array $listAnswers
     */
    protected function getUserNameForListAnswer(array $listAnswers)
    {
        $listUserId = [];

        foreach ($listAnswers as $answer) {
            array_push($listUserId, $answer->user_id);
        }

        $listUserName = $this->user->getListUserIdNameByListId($listUserId);

        foreach ($listAnswers as &$answer) {
            $answer->user_name = $listUserName[$answer->user_id];
        }

        return $listAnswers;
    }

    /**
     * Get detail of an answer
     * 
     * @param int $userId
     * @param string $answerId
     */
    public function getDetailAnswer(int $userId, string $answerId)
    {
        $uri = '/api/answers/' . $answerId . '/detail';

        $data = [
            'user_id' => $userId
        ];

        try {
            $response = $this->guzzleService->post($uri, $data);

            if ($response->code === 200) {
                return [
                    'status' => true,
                    'data' => $this->getAnswererInfo($response->data)
                ];
            } else {
                return [
                    'status' => false,
                    'message' => $response->message
                ];
            }
        } catch (\Exception $e) {
            $this->logService->writeLogException('Call question service to get detail answer', $e);

            return [
                'status' => false,
                'message' => trans('server_response.server_error'),
            ];
        }
    }

    /**
     * Get needed infomation for answerer
     * 
     * @param $answerInfo
     */
    protected function getAnswererInfo($answerInfo)
    {
        $userInfo = $this->user->getUserById($answerInfo->answer->user_id);

        $answerInfo->answer->user_name = $userInfo->last_name . $userInfo->first_name;

        $additionalInfo = $userInfo->additionalInfo;

        $answerInfo->answer->total_answers = $additionalInfo->total_answers;
        $answerInfo->answer->total_questions = $additionalInfo->total_questions;

        return $answerInfo;
    }

    public function getAllFieldOfAnUser(int $userId)
    {
        $uri = '/api/questions/users/' . $userId . '/fields';

        try {
            $response = $this->guzzleService->get($uri);

            return $response->data;
        } catch (\Exception $e) {
            $this->logService->writeLogException('Call question service to get all field answer', $e);

            return [
                'status' => false,
                'message' => trans('server_response.server_error'),
            ];
        }
    }

    /**
     * Call to update status of answer
     * 
     * @param string $answerId
     * @param string $questionId
     * @param int $userId
     * @param int $status
     */
    public function updateStatusAnswer(string $answerId, string $questionId, int $userId, int $status)
    {
        $uri = '/api/answers/update-status';

        $data = [
            "user_id" => $userId,
            "answer_id" => $answerId,
            "question_id" => $questionId,
            "status" => $status
        ];

        try {
            $response = $this->guzzleService->post($uri, $data);

            if ($response->code === 203) {
                return [
                    'status' => true,
                    'data' => $response->data->user_id,
                ];
            } else {
                return [
                    'status' => false,
                    'message' => $response->message
                ];
            }
        } catch (\Exception $e) {
            $this->logService->writeLogException('Call question service to update status answer', $e);

            return [
                'status' => false,
                'message' => trans('server_response.server_error'),
            ];
        }
    }

    /**
     * Get list question of Group
     *
     * @param int $groupId
     * @param $groupSectionId this field can be null
     */
    public function getListQuestionOfGroup(int $groupId, $groupSectionId)
    {
        if (is_null($groupSectionId)) {
            $uri = '/api/questions/group/' . $groupId . '/0';
        } else {
            $uri = '/api/questions/group/' . $groupId . '/' . $groupSectionId;
        }

        try {
            $response = $this->guzzleService->get($uri);

            if ($response->code === 200) {
                return [
                    'status' => true,
                    'data' => $response->data
                ];
            } else {
                return [
                    'status' => false,
                    'message' => $response->message
                ];
            }
        } catch (\Exception $e) {
            $this->logService->writeLogException('Call question service to get list question of group', $e);

            return [
                'status' => false,
                'message' => trans('server_response.server_error'),
            ];
        }
    }

    /**
     * Get list question of User in Group
     * 
     * @param int $userId
     * @param int $groupId
     */
    public function getListQuestionOfUserInGroup(int $userId, int $groupId)
    {
        $uri = '/api/questions/group/' . $groupId . '/user/' . $userId;

        try {
            $response = $this->guzzleService->get($uri);

            if ($response->code === 200) {
                return [
                    'status' => true,
                    'data' => $response->data
                ];
            } else {
                return [
                    'status' => false,
                    'message' => $response->message
                ];
            }
        } catch (\Exception $e) {
            $this->logService->writeLogException('Call question service to get list question of user in group', $e);

            return [
                'status' => false,
                'message' => trans('server_response.server_error'),
            ];
        }
    }

    /**
     * Get info of questions and answers of an user
     * 
     * @param int $userId
     */
    public function getUserInfo(int $userId)
    {
        $uri = '/api/questions/users/' . $userId . '/user-info';

        try {
            $response = $this->guzzleService->get($uri);

            if ($response->code === 200) {
                return [
                    'status' => true,
                    'data' => $response->data
                ];
            } else {
                return [
                    'status' => false,
                    'message' => $response->message
                ];
            }
        } catch (\Exception $e) {
            $this->logService->writeLogException('Call question service to get user info', $e);

            return [
                'status' => false,
                'message' => trans('server_response.server_error'),
            ];
        }
    }

    /**
     * Call to question service to update report status
     * 
     * @param string $answerId
     * @param int $status
     */
    public function updateReportStatus(string $answerId, int $status)
    {
        $uri = '/api/answers/' . $answerId . '/update-report-status';

        $data = [
            "status" => $status
        ];

        return $this->callPostMethodInGuzzle($uri, $data, 203, 'Call question service to update report status');
    }

    /**
     * Call to get question by list answerId
     */
    public function getListQuestionByListAnswerId(array $listId)
    {
        $uri = '/api/answers/list-question-by-answer-id';

        $data = [
            'list_question_id' => $listId
        ];

        return $this->callPostMethodInGuzzle($uri, $data, 200, 'Call to question service to get list question');
    }

    /**
     * Update answer status for admin
     * 
     * @param string $answerId
     * @param string $questionId
     * @param int $status
     */
    public function updateAnswerStatusForAdmin(string $answerId, string $questionId, int $status)
    {
        $uri = '/api/answers/admin/update-answer-status';

        $data = [
            'answer_id' => $answerId,
            'status' => $status,
            'question_id' => $questionId
        ];

        return $this->callPostMethodInGuzzle($uri, $data, 203, 'Call to question service to update answer status for admin');
    }

    /**
     * Update price for question
     * 
     * @param int $userId
     * @param string $questionId
     * @param int $price
     */
    public function updatePriceForQuestion(int $userId, string $questionId, int $price)
    {
        $uri = '/api/questions/change-price';

        $data = [
            'user_id' => $userId,
            'question_id' => $questionId,
            'price' => $price
        ];

        return $this->callPostMethodInGuzzle($uri, $data, 203, 'Call to question service to update price question');
    }

    /**
     * Update visibility for question
     * 
     * @param int $userId
     * @param string $questionId
     * @param int $visibility
     */
    public function updateVisibilityForQuestion(int $userId, string $questionId, int $visibility)
    {
        $uri = '/api/questions/change-visibility';

        $data = [
            'user_id' => $userId,
            'question_id' => $questionId,
            'visibility' => $visibility
        ];

        return $this->callPostMethodInGuzzle($uri, $data, 203, 'Call to question service to update visibility question');
    }

    /**
     * Update visibility for question
     * 
     * @param int $userId
     * @param string $questionId
     * @param int $fieldId
     */
    public function updateFieldIdQuestion(int $userId, string $questionId, int $fieldId)
    {
        $uri = '/api/questions/change-field';

        $data = [
            'user_id' => $userId,
            'question_id' => $questionId,
            'field_id' => $fieldId
        ];

        return $this->callPostMethodInGuzzle($uri, $data, 203, 'Call to question service to update field question');
    }

    /**
     * Update tag for question
     * 
     * @param int $userId
     * @param string $questionId
     * @param array $tags
     */
    public function updateTagsQuestion(int $userId, string $questionId, array $tags)
    {
        $uri = '/api/questions/update-tags';

        $data = [
            'user_id' => $userId,
            'question_id' => $questionId,
            'tags' => $tags
        ];

        return $this->callPostMethodInGuzzle($uri, $data, 203, 'Call to question service to update tags question');
    }

    /**
     * Update content for question
     * 
     * @param int $userId
     * @param string $questionId
     * @param array $content
     */
    public function updateContentQuestion(int $userId, string $questionId, array $content)
    {
        $uri = '/api/questions/change-content';

        $data = [
            'user_id' => $userId,
            'question_id' => $questionId,
            'content' => $content
        ];

        return $this->callPostMethodInGuzzle($uri, $data, 203, 'Call to question service to update content question');
    }

    /**
     * Find question with multiple conditions
     * 
     * @param string $title nullable
     * @param string $content nullable
     * @param array $tags nullable
     * @param int $priceFrom nullable
     * @param int $priceTo nullable
     * @param int $userId nullable
     * @param int $fieldId nullable
     * @param int $page
     */
    public function findQuestion($title, $content, $tags, int $priceFrom, int $priceTo, $userId, $fieldId, $page)
    {
        $title = $title === '' ? null : $title;
        $content = $content === '' ? null : $content;
        $priceFrom = $priceFrom === 0 ? null : $priceFrom;
        $priceTo = $priceTo === 0 ? null : $priceTo;
        $fieldId = $fieldId === 0 ? null : $fieldId;

        $uri = '/api/questions/find-question?page=' . ($page ? $page : 1);

        $data = [
            "title" => $title,
            "content" => $content,
            "tags" => $tags,
            "price_from" => $priceFrom,
            "price_to" => $priceTo,
            "user_id" => $userId,
            "field_id" => $fieldId
        ];

        return $this->callPostMethodInGuzzle($uri, $data, 200, 'Call to question service to find question');
    }

    public function getAllAnswerOfUser(int $userId, $page)
    {
        $uri = '/api/answers/all/' . $userId . '?page=' . (is_null($page) ? 1 : $page);

        return $this->callGetMethodInGuzzle($uri, 200, 'Call to question service to get all answer of user');
    }
}

<?php

return [
    'server_error' => 'Server error',

    /**
     * Groups
     */
    'invited_key_is_missing' => 'Invited key is missing',
    'group_not_found_by_invited_key' => 'Group not found by invited key',
    'join_group_successfully' => 'Join group successfully',
    'request_forbidden' => 'Request is forbidden',
    'delete_member_successfully' => 'Delete member successfully',
    'can_not_delete_yourself' => 'You can not delete yourself',
    'add_user_successfully' => 'User is added successfully',
    'group_id_has_been_taken' => 'The group id has been taken',
    'add_answer_successfully' => 'Add answer successfully',

    /**
     * Users
     */
    'user_not_found' => 'User not found',
    'create_question_successfullly' => 'Create question successfully',
    'buy_coin_successfully' => 'Buy coin successfully',
    'remain_coin_is_not_enough' => 'Số coin còn lại không đủ',
    'update_status_answer_successfully' => 'Cập nhật trạng thái câu trả lời thành công',
    'create_report_successfully' => 'Báo cáo nội dung thành công',
    'question_update_price_successfully' => 'Cập nhật giá câu hỏi thành công',
    'question_update_visibility_successfully' => 'Cập nhật cài đặt hiển thị của câu hỏi thành công',
    'question_update_field_successfully' => 'Cập nhật lĩnh vực cho câu hỏi thành công',
    'question_update_tags_successfully' => 'Cập nhật thẻ cho câu hỏi thành công',
    'question_update_content_successfully' => 'Cập nhật nội dung câu hỏi thành công',
    'compensate_successfully' => 'Đền bù thành công',

    /**
     * Admin
     */
    'not_admin' => 'Không có quyền truy cập nội dung',
    'report_not_found' => 'Không tìm thấy báo cáo',
    'report_was_taken' => 'Báo cáo đã có người nhận',
    'report_accept_successfully' => 'Báo cáo đã được lấy thành công',
    'report_accept_answer_is_true_successfully' => 'Cập nhật trạng thái câu trả lời thành công',
];

<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'namespace' => 'Auth'
], function () {
    Route::post('/register', 'RegisterController@main');
    Route::post('/login', 'LoginController@main');
});

Route::group([
    'middleware' => 'auth:api'
], function () {
    Route::group([
        'prefix' => 'groups',
        'namespace' => 'Groups'
    ], function () {
        Route::post('/create-group', 'CreateGroupController@main');
        Route::get('/by-invited-key', 'GetGroupByInvitedKeyController@main');
        Route::post('/join-group', 'JoinGroupByInvitedKeyController@main');
        Route::get('/index-sections', 'IndexSectionsOfCurrentUser@main');
        Route::get('/newest-member', 'GetNewestMemberOfGroup@main');
        Route::get('/questions/{groupId}', 'GetAllQuestionOfGroupController@main');
        Route::get('/questions/{groupId}/user/{userId}', 'GetAllQuestionOfUserInGroupController@main');
        Route::post('/question', 'CreateQuestionForGroupController@main');
        Route::post('/answer/update-status', 'UpdateStatusAnswerInGroupController@main');
        Route::get('/info', 'GetInfoGroupByAdminController@main');

        Route::group([
            'middleware' => 'checkGroupCreator'
        ], function () {
            Route::post('/add-member', 'AddMemberByAdmin@main');
            Route::post('/delete-member', 'DeleteMemberInGroupController@main');
            Route::post('/refresh-key', 'RefreshKeyController@main');
        });
    });

    Route::group([
        'prefix' => 'users',
        'namespace' => 'Users'
    ], function () {
        Route::get('/groups', 'IndexGroupController@main');
        Route::get('/basic-info', 'GetBasicInfo@main');
        Route::get('/all-info', 'GetAllInfoController@main');
        Route::get('/questions', 'GetQuestionController@main');
        Route::get('/question/{questionId}', 'GetQuestionDetailController@main');
        Route::post('/question', 'CreateQuestionController@main');
        Route::post('/question/update-price', 'UpdatePriceQuestionController@main');
        Route::post('/question/updade-visibility', 'UpdateVisibilityQuestionController@main');
        Route::post('/question/update-field', 'UpdateFieldQuestionController@main');
        Route::post('/question/update-tags', 'UpdateTagsQuestionController@main');
        Route::post('/question/update-content', 'UpdateContentQuestionController@main');
        Route::get('/answers/{questionId}', 'GetListAnswerOfQuestionController@main');
        Route::get('/all-answers', 'GetAllAnswerOfUserController@main');
        Route::get('/answer/{answerId}', 'GetDetailAnswerController@main');
        Route::post('/answer/update-status', 'UpdateStatusAnswerController@main');
        Route::post('/answer/report-rejected-answer', 'UpdateReportStatusAnswerController@main');
        Route::post('/buy-coin', 'BuyCoinController@main');
        Route::get('/coin-info', 'GetCoinInfoController@main');
        Route::post('/add-comment', 'AddAnswerController@main');
        Route::get('/fields', 'GetAllFieldOfUserController@main');
        Route::get('/coin-transactions', 'GetCoinTransactionController@main');
        Route::get('/chart/receiving-by-month', 'GetReceivingCoinByMonthController@main');
        Route::get('/chart/spending-by-month', 'GetSpendingCoinMonthlyController@main');
        Route::get('/chart/receiving-and-spending-by-month', 'GetReceivingAndSpendingCoinMonthlyController@main');
        Route::get('/reports', 'GetWarningOfUserController@main');
        Route::get('/history-confirm-answer', 'GetHistoryConfirmAnswerController@main');
        Route::post('/compensate', 'CompensateForMisjudgingUserController@main');
        Route::get('/all-groups', 'GetallGroupOfUserController@main');
        Route::get('/all-reports', 'GetAllReportsOfUserController@main');
        Route::get('/all-warning', 'GetAllWarningOfUserController@main');
    });

    Route::group([
        'prefix' => 'admin',
        'namespace' => 'Admin',
        'middleware' => 'checkRoleAdmin',
    ], function () {
        Route::get('/info', 'GetInfoAdminController@main');
        Route::get('/open-report', 'GetOpenReportForAdminController@main');
        Route::get('/reports', 'GetReportOfCurrentAdminController@main');
        Route::post('/accept-report', 'AcceptReportToResolveController@main');
        Route::post('/confirm-report', 'ConfirmReportController@main');
    });
});

Route::group([
    'prefix' => 'global',
    'namespace' => 'Globals',
], function () {
    Route::get('/top-users', 'GetTopUsersController@main');
    Route::get('/top-fields', 'GetTopFieldController@main');
    Route::get('/newest-questions', 'GetNewestQuestionController@main');
    Route::get('/user/{userId}', 'FindUserController@main');
    Route::get('/fields', 'GetListFieldController@main');
    Route::post('/find-question', 'FindQuestionController@main');
    Route::get('/question-detail/{questionId}', 'GetQuestionDetailController@main');
    Route::get('/answers/{questionId}', 'GetAnswersQuestionController@main');
});
